import axios from 'axios';
import qs from 'qs';
import moment from 'moment-timezone';

export function userEmailCheck(email) {
  return axios.get(`/api/public/emailChk?email=${email}`);
}

export function userNicknameCheck(nickname) {
  return axios.get(`/api/public/nicknameChk?nickname=${nickname}`);
}

export function requestAuthSms(phoneNum) {
  return axios.post(
    `/api/public/requestAuthSms`,
    qs.stringify({
      phoneNum,
    })
  );
}

export function userJoin(
  email,
  pwd,
  name,
  nickname,
  phoneNum,
  phoneAuthKey,
  bankAuthCode
) {
  return axios.post(
    `/api/userJoin`,
    qs.stringify({
      email,
      pwd,
      name,
      nickname,
      phoneNum,
      phoneAuthKey,
      bankAuthCode,
      bankCallbackUri: process.env.REACT_APP_OPEN_BANK_REDIRECT,
    })
  );
}

export function checkAuthSms(phoneNum, keyNum) {
  return axios.post(
    `/api/public/requestAuthSms/check`,
    qs.stringify({
      phoneNum,
      keyNum,
    })
  );
}

export function requestReAuthSms(phoneNum) {
  return axios.post(
    `/api/public/requestReAuthSms`,
    qs.stringify({
      phoneNum,
    })
  );
}

export function checkReAuthSms(phoneNum, keyNum) {
  return axios.post(
    `/api/public/requestReAuthSms/check`,
    qs.stringify({
      phoneNum,
      keyNum,
    })
  );
}

export function changeEmail(email) {
  return axios.put(
    `/api/user/emailChange`,
    qs.stringify({
      email,
    })
  );
}

export function changePwd(oldPw, newPw) {
  return axios.put(
    `/api/user/pwChange`,
    qs.stringify({
      oldPw,
      newPw,
    })
  );
}

export function changePhoneNum(phoneNum, key, pwd) {
  return axios.put(
    `/api/user/phoneNumberChange`,
    qs.stringify({
      phoneNum,
      key,
      pwd,
    })
  );
}

export function changeNickname(newNickName) {
  return axios.put(
    `/api/user/nickChange`,
    qs.stringify({
      newNickName,
    })
  );
}

export function changeIntroduction(intro) {
  return axios.put(
    `/api/user/introChange`,
    qs.stringify({
      intro,
    })
  );
}

export function changeBanking(bankName, fintech) {
  return axios.put(
    `/api/helper/banking`,
    qs.stringify({
      bankName,
      fintech,
    })
  );
}

export function changeAddress(address) {
  return axios.put(
    `/api/helper/address`,
    qs.stringify({
      address,
    })
  );
}

export function leaveMember(pwd) {
  return axios.delete('/api/user', {
    headers: {
      pwd,
    },
  });
}

export function adminEmailCheck(email) {
  return axios.get(`/api/public/adminEmailChk?email=${email}`);
}

export function adminNicknameCheck(nickname) {
  return axios.get(`/api/public/adminNicknameChk?nickname=${nickname}`);
}

export function adminPhoneNumCheck(phoneNum) {
  return axios.get(`/api/public/adminPhoneNumChk?phoneNum=${phoneNum}`);
}

export function addAdmin(email, pwd, name, nickname, phoneNum) {
  return axios.post(
    `/api/admin/add`,
    qs.stringify({
      email,
      pwd,
      name,
      nickname,
      phoneNum,
    })
  );
}

export function changeEmailAdmin(email) {
  return axios.put(
    `/api/admin/emailChange`,
    qs.stringify({
      email,
    })
  );
}

export function changePwdAdmin(pwd, newPwd) {
  return axios.put(
    `/api/admin/changePwd`,
    qs.stringify({
      pwd,
      newPwd,
    })
  );
}

export function changePhoneNumAdmin(phoneNum, pwd) {
  return axios.put(
    `/api/admin/phoneNumberChange`,
    qs.stringify({
      phoneNum,
      pwd,
    })
  );
}

export function changeNicknameAdmin(newNickName) {
  return axios.put(
    `/api/admin/nickChange`,
    qs.stringify({
      newNickName,
    })
  );
}

export function leaveAdmin(pwd) {
  return axios.delete('/api/admin', {
    headers: {
      pwd,
    },
  });
}

export function writeRequest(
  requestTitle,
  description,
  category,
  payment,
  isCanSuggestPayment,
  deadline,
  address,
  address2
) {
  return axios.post(
    '/api/user/request',
    qs.stringify({
      requestTitle,
      description,
      category,
      payment,
      isCanSuggestPayment,
      deadline: deadline ? moment(deadline).toDate() : undefined,
      address1: address.address,
      address2: address2,
      sidoCode: address.sidoCode,
      sigunguCode: address.sigunguCode,
      dongCode: address.bcode,
      axisX: address.axisX,
      axisY: address.axisY,
    })
  );
}

export function getRequest(requestId) {
  return axios.get(`/api/request/${requestId}`);
}

export function searchRequestList(
  page,
  sort,
  keyword,
  onlyWaiting,
  categories,
  minPrice,
  maxPrice,
  startX,
  startY,
  endX,
  endY
) {
  return axios.get('/api/public/request/search', {
    params: {
      page,
      sort: sort ?? undefined,
      keyword: keyword?.trim() ? keyword.trim() : undefined,
      onlyWaiting,
      categories,
      minPrice,
      maxPrice,
      startX,
      startY,
      endX,
      endY,
    },
    paramsSerializer: (params) => {
      return qs.stringify(params, { arrayFormat: 'repeat' });
    },
  });
}

export function myRequestList(page, states) {
  return axios.get('/api/user/request/list', {
    params: {
      page,
      states,
    },
    paramsSerializer: (params) => {
      return qs.stringify(params, { arrayFormat: 'repeat' });
    },
  });
}

export function getReportedRequestList(page) {
  return axios.get('/api/admin/request/list', {
    params: {
      page,
    },
    paramsSerializer: (params) => {
      return qs.stringify(params, { arrayFormat: 'repeat' });
    },
  });
}

export function deleteRequest(id) {
  return axios.delete(`/api/user/request/${id}`);
}

export function modifyRequest(
  id,
  requestTitle,
  description,
  category,
  payment,
  isCanSuggestPayment,
  deadline,
  address,
  address2
) {
  return axios.put(
    `/api/user/request/${id}`,
    qs.stringify({
      requestTitle,
      description,
      category,
      payment,
      isCanSuggestPayment,
      deadline: deadline ? moment(deadline).toDate() : undefined,
      address1: address.address,
      address2: address2,
      sidoCode: address.sidoCode,
      sigunguCode: address.sigunguCode,
      dongCode: address.bcode,
      axisX: address.axisX,
      axisY: address.axisY,
    })
  );
}

export function completeRequest(id) {
  return axios.put(`/api/user/request/${id}/complete`);
}

export function waitingRequest(id) {
  return axios.put(`/api/user/request/${id}/posted`);
}

export function refreshRequest(id) {
  return axios.put(`/api/user/request/${id}/refresh`);
}

export function reportRequest(id) {
  return axios.post(`/api/user/request/${id}/report`);
}

export function hideRequest(id) {
  return axios.put(`/api/admin/request/${id}/ignore`);
}

export function blockRequest(id) {
  return axios.put(`/api/admin/request/${id}/block`);
}

export function createChatRoom(partnerId) {
  return axios.post(
    '/api/user/chatting/room',
    qs.stringify({
      partnerId,
    })
  );
}

export function readChatRoom(roomId) {
  return axios.put(`/api/user/chatting/room/${roomId}/read`);
}

export function getChatMsgList(roomId, offsetId) {
  return axios.get(
    `/api/user/chatting/msg/list?roomId=${roomId}${
      offsetId ? `&offsetId=${offsetId}` : ''
    }`
  );
}

export function getChatRoomList(page) {
  return axios.get(`/api/user/chatting/room/list?page=${page}`);
}

export function readNoti() {
  return axios.put(`/api/user/noti/read`);
}

export function getNotiList(offsetId) {
  return axios.get(
    `/api/user/noti/list${offsetId ? `?offsetId=${offsetId}` : ''}`
  );
}

export function removeNoti(id) {
  return axios.delete(`/api/user/noti/${id}`);
}

export function clearNoti() {
  return axios.delete(`/api/user/noti`);
}

export function addReqApply(requestId, description, amount) {
  return axios.post(
    `/api/helper/requestApply`,
    qs.stringify({
      requestId,
      description,
      amount,
    })
  );
}

export function updateReqApply(requestId, description, amount) {
  return axios.put(
    `/api/helper/requestApply`,
    qs.stringify({
      requestId,
      description,
      amount,
    })
  );
}

export function getMyReqApplyList(page, states) {
  return axios.get(`/api/helper/requestApply`, {
    params: {
      page,
      states,
    },
    paramsSerializer: (params) => {
      return qs.stringify(params, { arrayFormat: 'repeat' });
    },
  });
}

export function getRequestPageApplyList(page, requestId) {
  return axios.get(`/api/requestApply/request/${requestId}`, {
    params: {
      page,
    },
    paramsSerializer: (params) => {
      return qs.stringify(params, { arrayFormat: 'repeat' });
    },
  });
}

export function deleteRequestApply(id) {
  return axios.delete(`/api/helper/requestApply/${id}`);
}

export function approveRequestApply(id, payMethod, bankName, fintech) {
  return axios.put(
    `/api/user/requestApply/${id}/approve`,
    qs.stringify({
      payMethod,
      bankName,
      fintech,
    })
  );
}

export function rejectRequestApply(id) {
  return axios.put(`/api/user/requestApply/${id}/reject`);
}

export function getRequestApply(id) {
  return axios.get(`/api/requestApply/${id}`);
}

export function getMyAccountList() {
  return axios.get(`/api/user/payment/account`);
}

export function reAuthBank(bankAuthCode, pwd) {
  return axios.put(
    `/api/user/bankReAuth`,
    qs.stringify({
      bankAuthCode,
      bankCallbackUri: process.env.REACT_APP_OPEN_BANK_REDIRECT,
      pwd,
    })
  );
}

export function getOrderInfo(id) {
  return axios.get(`/api/user/order/${id}`);
}

export function approveOrder(id) {
  return axios.put(`/api/helper/order/${id}/accept`);
}

export function cancelOrder(id) {
  return axios.put(`/api/user/order/${id}/cancel`);
}

export function completeOrder(id) {
  return axios.put(`/api/user/order/${id}/complete`);
}

export function getMyReqestOrderList(page, states) {
  return axios.get(`/api/user/order/list`, {
    params: {
      page,
      states,
    },
    paramsSerializer: (params) => {
      return qs.stringify(params, { arrayFormat: 'repeat' });
    },
  });
}

export function getMyHelperOrderList(page, states) {
  return axios.get(`/api/helper/order/list`, {
    params: {
      page,
      states,
    },
    paramsSerializer: (params) => {
      return qs.stringify(params, { arrayFormat: 'repeat' });
    },
  });
}

export function retryRefund(id) {
  return axios.put(`/api/user/order/${id}/refund`);
}

export function retryRefundAll(bankName, fintech) {
  return axios.put(
    `/api/user/order/refund`,
    qs.stringify({
      bankName,
      fintech,
    })
  );
}

export function retryCalc(id) {
  return axios.put(`/api/helper/order/${id}/calc`);
}

export function retryCalcAll(bankName, fintech) {
  return axios.put(
    `/api/helper/order/calc`,
    qs.stringify({
      bankName,
      fintech,
    })
  );
}

export function registerHelper(address, idCard, bankName, fintechNum) {
  const formData = new FormData();
  formData.set('address', address);
  formData.set('idCard', idCard);
  formData.set('bankName', bankName);
  formData.set('fintechNum', fintechNum);

  return axios.post(`/api/user/helperApply`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
}

export function getRegisterHelperState() {
  return axios.get(`/api/user/helperApply/state`);
}

export function cancelRegisterHelper() {
  return axios.delete(`/api/user/helperApply`);
}

export function getHelperApplicants(page) {
  return axios.get('/api/admin/helperApplys', {
    params: {
      page,
    },
    paramsSerializer: (params) => {
      return qs.stringify(params, { arrayFormat: 'repeat' });
    },
  });
}

export function rejectRegisterHelper(id) {
  return axios.put(`/api/admin/helperApply/${id}/reject`);
}

export function approveRegisterHelper(id) {
  return axios.put(`/api/admin/helperApply/${id}/approve`);
}

export function getMyAcceptOfferList(page) {
  return axios.get(`/api/user/requestApply`, {
    params: {
      page,
    },
    paramsSerializer: (params) => {
      return qs.stringify(params, { arrayFormat: 'repeat' });
    },
  });
}

export function writeReview(orderId, content, evaluation) {
  return axios.post(
    `/api/user/review`,
    qs.stringify({
      orderId,
      content,
      evaluation,
    })
  );
}
