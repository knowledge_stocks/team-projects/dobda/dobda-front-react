import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';

import { BrowserRouter, Routes, Route } from 'react-router-dom';

import PublicHoc from './hoc/public';
import AuthHoc from './hoc/auth';
import HelperHoc from './hoc/helper';
import AdminHoc from './hoc/admin';

import MainNav from './components/main_layouts/MainNav';
import SignIn from './components/login_layouts/SignIn';
import RequestResults from './components/request/RequestResults';
import SignUp from './components/signup_layouts/SignUp';
import Write from './components/write_layouts/Write';
import View from './components/write_layouts/View';
import Footer from './components/write_layouts/Footer';
import AuthCallback from './components/banking/AuthCallback';
import MyPage from './components/my_page/MyPage';
import MyInfo from './components/my_page/MyInfo';
import MyRequest from './components/my_page/MyRequest';
import MyOrder from './components/my_page/MyOrder';
import MyAceptOrder from './components/my_page/MyAceptOrder';
import MyApply from './components/my_page/MyApply';
import MyNoti from './components/my_page/MyNoti';
import Modify from './components/write_layouts/Modify';
import MyAcceptOffer from './components/my_page/MyAcceptOffer';
import AdminSignIn from './components/login_layouts/AdminSignIn';
import AdminMyPage from './components/admin_page/AdminMyPage';
import AdminMyInfo from './components/admin_page/AdminMyInfo';
import ReportedRequest from './components/admin_page/ReportedRequest';
import HelperApply from './components/admin_page/HelperApply';
import AdminSignUp from './components/signup_layouts/AdminSignUp';

import CheckOut from './components/helper_sign/CheckOut';
import ChatModalHandler from './components/chat/ChatModalHandler';

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route
            path='/'
            element={PublicHoc(
              <>
                <MainNav />
                <RequestResults />
                <Footer />
              </>
            )}
          />
          <Route
            path='/signIn'
            element={AuthHoc(
              <>
                <SignIn />
              </>,
              false
            )}
          />
          <Route
            path='/signUp'
            element={AuthHoc(
              <>
                <SignUp />
              </>,
              false
            )}
          />
          <Route
            path='/myPage'
            element={AuthHoc(
              <>
                <MainNav />
                <MyPage />
                <Footer />
              </>
            )}
          >
            <Route path='' element={<MyRequest />} />
            <Route path='noti' element={<MyNoti />} />
            <Route path='myInfo' element={<MyInfo />} />
            <Route path='myRequest' element={<MyRequest />} />
            <Route path='myOrder' element={<MyOrder />} />
            <Route path='myAceptOrder' element={<MyAceptOrder />} />
            <Route path='myOffer' element={<MyApply />} />
            <Route path='myAceptOffer' element={<MyAcceptOffer />} />
          </Route>
          <Route
            path='/write'
            element={AuthHoc(
              <>
                <MainNav />
                <Write />
                <Footer />
              </>
            )}
          />
          <Route
            path='/modify'
            element={AuthHoc(
              <>
                <MainNav />
                <Modify />
                <Footer />
              </>
            )}
          />
          <Route
            path='/view'
            element={AuthHoc(
              <>
                <MainNav />
                <View />
                <Footer />
              </>
            )}
          />
          <Route
            path='/openbank/auth'
            element={
              <>
                <AuthCallback />
              </>
            }
          />
          <Route
            path='/helper'
            element={HelperHoc(
              <>
                <MainNav />
                <CheckOut />
                <Footer />
              </>,
              false
            )}
          />
          // 여기부터 관리자용
          <Route
            path='/admin/signIn'
            element={AuthHoc(
              <>
                <AdminSignIn />
              </>,
              false
            )}
          />
          <Route
            path='/admin/signUp'
            element={PublicHoc(
              <>
                <AdminSignUp />
              </>,
              false
            )}
          />
          <Route
            path='/admin/myPage'
            element={AdminHoc(
              <>
                <MainNav />
                <AdminMyPage />
                <Footer />
              </>
            )}
          >
            <Route path='' element={<AdminMyInfo />} />
            <Route path='myInfo' element={<AdminMyInfo />} />
            <Route path='request' element={<ReportedRequest />} />
            <Route path='helper' element={<HelperApply />} />
          </Route>
        </Routes>
      </BrowserRouter>
      <ChatModalHandler />
    </>
  );
}

export default App;
