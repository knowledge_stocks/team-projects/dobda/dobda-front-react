import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { myInfo } from '../_redux/actions/user_actions';
import { resetAllChatInfo } from '../_redux/actions/modal_action';
import SockJS from 'sockjs-client';
import * as StompJs from '@stomp/stompjs';

export default function (WrappedComponent, option = true) {
  function AuthenticationCheck() {
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const isLoggedIn = useSelector((state) => state.user.isLoggedIn);
    const userInfo = useSelector((state) => state.user.userInfo);

    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
      if (isLoggedIn) {
        if (!option) {
          navigate('/');
          return;
        }
      } else {
        dispatch(resetAllChatInfo());
        if (!option) {
          setLoaded(true);
          return;
        }
        navigate('/signIn');
        return;
      }

      dispatch(myInfo()).catch(() => {
        navigate('/signIn');
      });
    }, [isLoggedIn]);

    useEffect(() => {
      if (!userInfo?.id) {
        return;
      }
      if (userInfo.isAdmin) {
        setLoaded(true);
        return;
      }
      const client = new StompJs.Client();
      client.webSocketFactory = function () {
        return new SockJS(process.env.REACT_APP_BACKEND_HOST + '/ws');
      };
      client.onConnect = () => {
        client.subscribe(`/ws/topic/${userInfo.id}/**`, () => {
          dispatch(myInfo());
        });
      };
      client.activate();
      setLoaded(true);

      return () => {
        client.deactivate();
      };
    }, [userInfo?.id]);

    return loaded && WrappedComponent;
    // return WrappedComponent;
  }

  return <AuthenticationCheck />;
}
