import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { myInfo } from '../_redux/actions/user_actions';
import { resetAllChatInfo } from '../_redux/actions/modal_action';

// 관리자만 접근 가능
export default function (WrappedComponent) {
  function AdminCheck() {
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const isLoggedIn = useSelector((state) => state.user.isLoggedIn);
    const userInfo = useSelector((state) => state.user.userInfo);

    const [isCheckedPermission, setIsCheckedPermission] = useState(false);

    useEffect(() => {
      if (!isLoggedIn) {
        navigate('/admin/signIn');
        return;
      }
      if (!userInfo || isCheckedPermission) {
        return;
      }

      if (!userInfo.isAdmin) {
        navigate('/admin/signIn');
        return;
      }
      setIsCheckedPermission(true);
      dispatch(resetAllChatInfo());

      dispatch(myInfo()).catch(() => {});
    }, [isLoggedIn, userInfo]);

    return WrappedComponent;
  }

  return <AdminCheck />;
}
