import { MAP_CENTER, MAP_ZOOM } from '../actions/types';

export default function (state = {}, action) {
  switch (action.type) {
    case MAP_CENTER:
      return { ...state, center: action.data };
    case MAP_ZOOM:
      return { ...state, zoom: action.data };
    default:
      return state;
  }
}
