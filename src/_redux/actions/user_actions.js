import axios from 'axios';
import qs from 'qs';
import { LOGIN, MY_INFO, LOGOUT, REAUTH } from './types';

export function loginUser(data) {
  const result = axios.post('/api/user/login', qs.stringify(data));

  return {
    type: LOGIN,
    payload: result,
  };
}

export function loginAdmin(data) {
  const result = axios.post('/api/admin/login', qs.stringify(data));

  return {
    type: LOGIN,
    payload: result,
  };
}

export function myInfo() {
  const result = axios.get('/api/myInfo').then((response) => response.data);

  return {
    type: MY_INFO,
    payload: result,
  };
}

export function logout() {
  return {
    type: LOGOUT,
  };
}

export function reAuth() {
  const result = axios.get('/api/user/reAuth').then(response => response.data);

  return {
    type: REAUTH,
    payload: result,
  };
}