export default [
  {
    code: 10,
    label: '모집중',
  },
  {
    code: 20,
    label: '거래중',
  },
  {
    code: 30,
    label: '완료',
  },
  {
    code: 40,
    label: '삭제됨',
  },
  {
    code: 60,
    label: '제재됨',
  },
];
