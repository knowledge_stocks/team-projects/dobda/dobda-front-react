export default [
  {
    code: 1,
    label: '만나서 직접 전달',
  },
  {
    code: 2,
    label: '계좌이체(DOBDA에서 대금 중계)',
  },
];
