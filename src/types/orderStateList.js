export default [
  {
    code: 0,
    label: '수락 대기',
  },
  {
    code: 10,
    label: '수락됨',
  },
  {
    code: 40,
    label: '완료',
  },
  {
    code: 50,
    label: '취소',
  },
  {
    code: 60,
    label: '취소(대금 처리중)',
  },
  {
    code: 70,
    label: '완료(대금 처리중)',
  },
];
