import './SignIn.css';
import React, { useState, useCallback } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import TextField from '@mui/material/TextField';
import Checkbox from '@mui/material/Checkbox';
import Button from '@mui/material/Button';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Card from '@mui/material/Card';
import { useDispatch } from 'react-redux';
import { loginUser } from '../../_redux/actions/user_actions';

function Copyright(props) {
  return (
    <Typography
      variant='body2'
      color='text.secondary'
      align='center'
      {...props}
    >
      {'Copyright © '}
      <Link color='inherit' to='/'>
        DOBDA
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

export default function SignIn() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [email, setEmail] = useState('');
  const [pwd, setPwd] = useState('');

  const onChangeEmail = useCallback((event) => {
    const email = event.target.value;
    setEmail(email);
  });

  const onChangePwd = useCallback((event) => {
    const pwd = event.target.value;
    setPwd(pwd);
  });

  const onClickLogin = useCallback(
    (e) => {
      e.preventDefault();
      if (email == '') {
        return alert('이메일을 입력해주세요.😥');
      }
      if (pwd == '') {
        return alert('비밀번호를 입력해주세요.😥');
      }

      dispatch(loginUser({ email: email, pwd: pwd }))
        .then(() => navigate('/'))
        .catch((err) => alert('로그인 요청이 실패 했습니다.'));
    },
    [email, pwd]
  );

  return (
    <Card id='login_card' sx={{ maxWidth: 500 }}>
      <Container component='main' maxWidth='xs'>
        <Box id='box'>
          <Typography
            id='BrandName'
            sx={{ mt: 8, mb: 3 }}
            component='h1'
            variant='h5'
          >
            DOBDA
          </Typography>
          <TextField
            margin='normal'
            label='이메일'
            required
            fullWidth
            value={email}
            onChange={onChangeEmail}
            autoComplete='email'
            autoFocus
            sx={{ mb: 2 }}
            // variant='standard'
          />
          <TextField
            label='비밀번호'
            type='password'
            required
            fullWidth
            value={pwd}
            onChange={onChangePwd}
            autoComplete='current-password'
          />
          {/* <FormControlLabel
            control={<Checkbox value='remember' color='primary' />}
            label='아이디 저장'
          /> */}
          <Button
            id='login_btn'
            type='submit'
            fullWidth
            variant='contained'
            sx={{ mt: 4, mb: 3 }}
            onClick={onClickLogin}
          >
            로그인
          </Button>
          <Grid container id='signup_btn'>
            <Grid item xs>
              <Link to='/signUp'>회원가입</Link>
            </Grid>
          </Grid>
        </Box>
        <Copyright sx={{ mt: 2, mb: 3 }} />
      </Container>
    </Card>
  );
}
