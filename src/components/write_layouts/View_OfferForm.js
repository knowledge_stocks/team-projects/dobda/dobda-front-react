import React, { useState, useEffect, useCallback } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import './View_OfferForm.css';
import { InputAdornment, TextField } from '@mui/material';
import TextareaAutosize from '@mui/material/TextareaAutosize';
import {
  getRequestApply,
  updateReqApply,
  addReqApply,
} from '../../axios/axios_actions';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 500,
  bgcolor: 'background.paper',
  border: '0px',
  boxShadow: 24,
  p: 4,
};

export default function View_OfferForm({
  open,
  onClose,
  onConfirm,
  requestId,
  applyId,
}) {
  const [description, setDescription] = useState('');
  const [amount, setAmount] = useState('');

  useEffect(() => {
    if (!requestId) {
      if (open) {
        alert('의뢰 정보를 받아올 수 없습니다.');
        onClose?.();
      }
    }
    if (!applyId) {
      return;
    }
    getRequestApply(applyId).then((response) => {
      const applyInfo = response.data;
      setDescription(applyInfo.description);
      setAmount(applyInfo.amount);
    });
  }, [open, applyId, onClose]);

  const onChangeDescription = useCallback(
    ({ target }) => setDescription(target.value),
    []
  );
  const onChangeAmount = useCallback(
    ({ target }) => setAmount(target.value),
    []
  );
  const onClickConfirm = useCallback(async () => {
    if (!applyId) {
      try {
        const response = await addReqApply(requestId, description, amount);
        onConfirm?.(response.data);
        onClose?.();
        return;
      } catch (err) {
        if (err.response) {
          if (err.response.data == '중복') {
            if (
              !window.confirm(
                '이미 등록된 제안이 있습니다.\n현재 내용으로 덮어 쓰시겠습니까?'
              )
            ) {
              return;
            }
          } else {
            alert(err.response.data);
            return;
          }
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
          return;
        } else {
          alert('잘못된 요청입니다.');
          return;
        }
      }
    }
    try {
      const response = await updateReqApply(requestId, description, amount);
      onConfirm?.(response.data);
      onClose?.();
      return;
    } catch (err) {
      if (err.response) {
        alert(err.response.data);
      } else if (err.request) {
        alert('서버가 응답하지 않습니다.');
      } else {
        alert('잘못된 요청입니다.');
      }
    }
  }, [applyId, requestId, description, amount]);

  return (
    <Modal
      open={open}
      aria-labelledby='modal-modal-title'
      aria-describedby='modal-modal-description'
    >
      <Box className='OfferForm_wrap' sx={style}>
        <Typography
          id='OfferForm_title'
          variant='h6'
          component='h2'
          textAlign='center'
        >
          제안하기
        </Typography>
        <TextField
          className='OfferForm_price'
          type='number'
          label='제안 가격을 입력해 주세요'
          sx={{ m: 1, width: 400, mb: 2, mt: 2, ml: 2 }}
          InputProps={{
            startAdornment: <InputAdornment position='start'>₩</InputAdornment>,
          }}
          value={amount}
          onChange={onChangeAmount}
        />
        <TextareaAutosize
          className='OfferForm_content'
          aria-label='minimum height'
          maxLength='200'
          minRows={10}
          placeholder='내용을 입력해 주세요'
          style={{ width: 400, m: 1 }}
          value={description}
          onChange={onChangeDescription}
        />

        <Button
          className='OfferForm_cancelbtn'
          variant='contained'
          sx={{ mr: 2.5, mt: 1 }}
          onClick={onClose}
        >
          취소
        </Button>
        <Button
          className='OfferForm_finalbtn'
          variant='contained'
          sx={{ mr: 2.5, mt: 1 }}
          onClick={onClickConfirm}
        >
          등록
        </Button>
      </Box>
    </Modal>
  );
}
