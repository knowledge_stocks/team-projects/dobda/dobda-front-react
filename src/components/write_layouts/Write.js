import React, { useState, useEffect, useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import './Write.css';
import Button from '@mui/material/Button';
import { useQuill } from 'react-quilljs';
import 'quill/dist/quill.snow.css';
import TextField from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';
import Switch from '@mui/material/Switch';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import LocationSearchingIcon from '@mui/icons-material/LocationSearching';
import {
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
} from '@mui/material';
import categories from '../../types/category';
import sidoList from '../../types/sidoList';
import DaumPostModal from '../common/DaumPostModal';
import { writeRequest } from '../../axios/axios_actions';

const { kakao } = window;

export default function Write() {
  const { quill, quillRef } = useQuill();
  const navigate = useNavigate();

  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [descriptionText, setDescriptionText] = useState('');
  const [category, setCategory] = useState('');
  const [payment, setPayment] = useState('');
  const [canSuggest, setCanSuggest] = useState(false);
  const [deadline, setDeadline] = useState('');
  const [address, setAddress] = useState();
  const [address2, setAddress2] = useState('');
  const [openDaumModal, setOpenDaumModal] = useState(false);
  const [kakaoMapInfo, setKakaoMapInfo] = useState();

  useEffect(() => {
    const container = document.getElementsByClassName('Write_map')[0];
    const options = {
      center: new kakao.maps.LatLng(37.566134, 126.977808),
      level: 3,
      draggable: false,
      zoomable: false,
    };

    const map = new kakao.maps.Map(container, options);

    const geocoder = new kakao.maps.services.Geocoder();
    const marker = new kakao.maps.Marker({
      position: new kakao.maps.LatLng(37.566134, 126.977808),
      map: map,
    });

    setKakaoMapInfo({
      map,
      geocoder,
      marker,
    });
  }, []);

  useEffect(() => {
    if (!kakaoMapInfo || !address) {
      return;
    }

    const coords = new kakao.maps.LatLng(address.axisY, address.axisX);
    kakaoMapInfo.map.relayout();
    kakaoMapInfo.map.setCenter(coords);
    kakaoMapInfo.marker.setPosition(coords);
  }, [kakaoMapInfo, address]);

  useEffect(() => {
    if (quill) {
      quill.on('text-change', (delta, oldDelta, source) => {
        setDescription(quill.root.innerHTML);
        setDescriptionText(quill.root.textContent);
      });
    }
  }, [quill]);

  const onReturnAddress = useCallback(
    (data) => {
      if (!kakaoMapInfo) {
        return;
      }

      kakaoMapInfo.geocoder.addressSearch(
        data.address,
        function (results, status) {
          if (status === kakao.maps.services.Status.OK) {
            const result = results[0];
            data.axisX = result.x;
            data.axisY = result.y;
            data.sidoCode = sidoList.find((v) =>
              v.value.find((v) => v == data.sido)
            ).code;

            setAddress(data);
          } else {
            alert('좌표를 찾을 수 없습니다.');
            return;
          }
        }
      );
    },
    [kakaoMapInfo]
  );

  const onChangeTitle = useCallback(({ target }) => setTitle(target.value), []);
  const onChangeCategory = useCallback(
    ({ target }) => setCategory(target.value),
    []
  );
  const onChangePayment = useCallback(
    ({ target }) => setPayment(target.value),
    []
  );
  const onChangeCanSuggest = useCallback(
    ({ target }) => setCanSuggest(target.checked),
    []
  );
  const onChangeDeadline = useCallback(
    ({ target }) => setDeadline(target.value),
    []
  );
  const onChangeAddress2 = useCallback(
    ({ target }) => setAddress2(target.value),
    []
  );

  const onClickSubmit = useCallback(() => {
    if (category !== 0 && !category) {
      return alert('카테고리를 선택해주세요.😥');
    }
    if (!title) {
      return alert('제목을 입력해주세요.😥');
    }
    if (!descriptionText) {
      return alert('내용을 입력해주세요.😥');
    }
    if (!payment) {
      return alert('가격을 입력해주세요.😥');
    }
    if (!address) {
      return alert('위치를 선택해주세요.😥');
    }

    writeRequest(
      title,
      description,
      category,
      payment,
      canSuggest,
      deadline,
      address,
      address2
    )
      .then((response) => {
        alert('의뢰글이 작성되었습니다.😊');
        navigate(`/view?id=${response.data}`);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [
    title,
    description,
    descriptionText,
    category,
    payment,
    canSuggest,
    deadline,
    address,
    address2,
  ]);

  return (
    <div className='Write_root'>
      <div className='Write_story-content'>
        <div className='write_subwrap'>
          <TextField
            className='Write_datePicker'
            label='마감 날짜'
            type='date'
            InputLabelProps={{
              shrink: true,
            }}
            value={deadline}
            onChange={onChangeDeadline}
          />
          <TextField
            className='write_category'
            select
            label='카테고리를 선택해 주세요.'
            value={category}
            onChange={onChangeCategory}
          >
            {categories.map((option) => (
              <MenuItem key={option.code} value={option.code}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </div>

        <div className='write_form-wrapper'>
          <TextField
            label='제목을 입력해 주세요.'
            variant='outlined'
            fullWidth
            value={title}
            onChange={onChangeTitle}
          />

          {/* <div className='write_description' ref={quillRef} /> */}
          <div className='write_description'>
            <div ref={quillRef} />
          </div>

          <div className='write_address'>
            <FormControl fullWidth required>
              <InputLabel htmlFor='outlined-adornment-address'>주소</InputLabel>
              <OutlinedInput
                endAdornment={
                  <InputAdornment position='end'>
                    <IconButton>
                      <LocationSearchingIcon />
                    </IconButton>
                  </InputAdornment>
                }
                label='주소'
                value={address?.address}
                sx={{ mb: '10px' }}
                onClick={() => setOpenDaumModal(true)}
                readOnly
              />
            </FormControl>
            {openDaumModal && (
              <DaumPostModal
                onClose={() => setOpenDaumModal(false)}
                onComplete={onReturnAddress}
                closeWhenClickBack
              />
            )}
            <TextField
              label='상세 주소'
              variant='outlined'
              fullWidth
              value={address2}
              onChange={onChangeAddress2}
            />
            <div
              className={
                'Write_map' + (address?.address ? '' : ' Write_map_hidden')
              }
            ></div>
          </div>

          <div className='write_price'>
            <TextField
              type='number'
              label='가격을 입력해주세요.'
              variant='outlined'
              value={payment}
              onChange={onChangePayment}
            />
            <FormControlLabel
              className='write_can_suggest'
              control={
                <Switch
                  color='primary'
                  checked={canSuggest}
                  onChange={onChangeCanSuggest}
                />
              }
              label='초과 제안 받기'
              sx={{ ml: '10px' }}
            />
            <Button
              variant='contained'
              className='write_submit-button'
              onClick={onClickSubmit}
            >
              작성
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}
