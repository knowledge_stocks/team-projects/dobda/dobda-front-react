import './View.css';
import React, { useCallback, useState, useEffect } from 'react';
import DriveFileRenameOutline from '@mui/icons-material/DriveFileRenameOutline';
import Button from '@mui/material/Button';
import Chip from '@mui/material/Chip';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMessage } from '@fortawesome/free-solid-svg-icons';
import View_OfferForm from './View_OfferForm';
import { useSearchParams, useNavigate, Link } from 'react-router-dom';
import requestStateList from '../../types/requestStateList';
import categoryList from '../../types/category';
import moment from 'moment-timezone';
import { timeForToday } from '../../util/utils';
import { useSelector, useDispatch } from 'react-redux';
import { createChatRoom } from '../../_redux/actions/modal_action';
import {
  getRequest,
  reportRequest,
  refreshRequest,
  deleteRequest,
  completeRequest,
  waitingRequest,
  hideRequest,
  blockRequest,
  getRequestPageApplyList,
} from '../../axios/axios_actions';
import ViewOffer from '../view_offer/ViewOffer';
import Loader from '../common/Loader';
import { useInView } from 'react-intersection-observer';

const { kakao } = window;

export default function View() {
  const dispatch = useDispatch();
  const userInfo = useSelector((state) => state.user.userInfo);

  const [searchParams] = useSearchParams();
  const navigate = useNavigate();

  const [requestInfo, setRequestInfo] = useState();
  const [state, setState] = useState();
  const [category, setCategory] = useState();
  const [isMine, setIsMine] = useState(false);
  const [openOfferModal, setOpenOfferModal] = useState(false);
  const [kakaoMapInfo, setKakaoMapInfo] = useState();
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const [applyList, setApplyList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(0);
  const [isEnd, setIsEnd] = useState(false);
  const [bottomRef, inView] = useInView();

  useEffect(() => {
    const container = document.getElementsByClassName('View_map')[0];
    const options = {
      center: new kakao.maps.LatLng(37.566134, 126.977808),
      level: 3,
    };

    const map = new kakao.maps.Map(container, options);

    const marker = new kakao.maps.Marker({
      position: new kakao.maps.LatLng(37.566134, 126.977808),
      map: map,
    });

    setKakaoMapInfo({
      map,
      marker,
    });

    const requestId = searchParams.get('id');
    if (!requestId) {
      alert('존재하지 않는 글입니다.');
      navigateScroll('/');
      return;
    }
    getRequest(requestId)
      .then((response) => {
        setRequestInfo(response.data);
        setCategory(
          categoryList.find((v) => v.code == response.data.category)?.label
        );
        setIsMine(
          !userInfo?.isAdmin && userInfo?.id == response.data.writerInfo.id
        );
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
        navigateScroll('/');
      });
  }, []);

  useEffect(() => {
    if (!kakaoMapInfo || !requestInfo) {
      return;
    }

    const coords = new kakao.maps.LatLng(requestInfo.axisY, requestInfo.axisX);
    kakaoMapInfo.map.relayout();
    kakaoMapInfo.map.setCenter(coords);
    kakaoMapInfo.marker.setPosition(coords);
  }, [kakaoMapInfo, requestInfo]);

  useEffect(() => {
    if (!requestInfo) {
      return;
    }
    setState(requestStateList.find((v) => v.code == requestInfo.state)?.label);
  }, [requestInfo]);

  useEffect(async () => {
    if (page === 0 || !requestInfo) {
      return;
    }
    setIsLoading(true);
    try {
      const response = await getRequestPageApplyList(
        page,
        requestInfo?.requestId
      );
      setApplyList((applyList) => [...applyList, ...response.data.content]);
      setIsEnd(response.data.last);
    } catch {
      setPage((prevState) => prevState - 1);
    }
    await new Promise((resolve) => setTimeout(resolve, 500));
    setIsLoading(false);
  }, [page, requestInfo]);

  useEffect(() => {
    if (inView && !isLoading && !isEnd && requestInfo) {
      setPage((prevState) => prevState + 1);
    }
  }, [inView, isLoading, isEnd, requestInfo]);

  const onFallbackImg = useCallback((event) => {
    event.currentTarget.onerror = null;
    event.currentTarget.src = '/user.png';
  });

  const handleClick = useCallback((event) => {
    setAnchorEl(event.currentTarget);
  }, []);

  const handleClose = useCallback(() => {
    setAnchorEl(null);
  }, []);

  const onClickMessage = useCallback(() => {
    dispatch(createChatRoom(requestInfo?.writerInfo.id));
  }, [requestInfo]);

  const onClickModify = useCallback(() => {
    handleClose();
    if (!window.confirm('수정 화면으로 이동하시겠습니까?')) {
      return;
    }
    navigateScroll(`/modify?id=${requestInfo.requestId}`);
  }, [requestInfo]);

  const onClickDelete = useCallback(() => {
    handleClose();
    if (!window.confirm('정말 삭제하시겠습니까?')) {
      return;
    }

    deleteRequest(requestInfo?.requestId)
      .then(() => {
        alert('의뢰글을 삭제했습니다.');
        navigateScroll('/');
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [requestInfo]);

  const onClickComplete = useCallback(() => {
    handleClose();
    if (!window.confirm('완료 상태로 변경하시겠습니까?')) {
      return;
    }

    completeRequest(requestInfo?.requestId)
      .then(() => {
        alert('완료로 변경되었습니다.');
        setRequestInfo((requestInfo) => {
          return { ...requestInfo, state: 30 };
        });
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [requestInfo]);

  const onClickWaiting = useCallback(() => {
    handleClose();
    if (!window.confirm('모집중 상태로 변경하시겠습니까?')) {
      return;
    }

    waitingRequest(requestInfo?.requestId)
      .then(() => {
        alert('모집중으로 변경되었습니다.');
        setRequestInfo((requestInfo) => {
          return { ...requestInfo, state: 10 };
        });
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [requestInfo]);

  const onClickRefresh = useCallback(() => {
    handleClose();

    refreshRequest(requestInfo?.requestId)
      .then(() => alert('의뢰글을 끌어올렸습니다.'))
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [requestInfo]);

  const onClickReport = useCallback(() => {
    handleClose();
    if (!window.confirm('이 의뢰글을 신고하시겠습니까?')) {
      return;
    }

    reportRequest(requestInfo?.requestId)
      .then(() => alert('의뢰글을 신고했습니다.'))
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [requestInfo]);

  const onClickHide = useCallback(() => {
    handleClose();
    if (!window.confirm('이 의뢰글을 신고 목록에서 제외하시겠습니까?')) {
      return;
    }

    hideRequest(requestInfo?.requestId)
      .then(() => alert('의뢰글을 신고 목록에서 제외했습니다.'))
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [requestInfo]);

  const onClickBlock = useCallback(() => {
    handleClose();
    if (!window.confirm('이 의뢰글을 불량글로 블럭 처리하시겠습니까?')) {
      return;
    }

    blockRequest(requestInfo?.requestId)
      .then(() => alert('의뢰글이 블럭되었습니다.'))
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [requestInfo]);

  const onDelete = useCallback((applyInfo) => {
    if (!applyInfo) {
      return;
    }
    setApplyList((applyList) => applyList.filter((v) => v.id != applyInfo.id));
  }, []);

  const onConfirm = useCallback((applyInfo) => {
    if (!applyInfo) {
      return;
    }
    setApplyList((applyList) => {
      const newList = applyList.filter((v) => v.id != applyInfo.id);
      newList.unshift(applyInfo);
      return newList;
    });
    handleClose();
  }, []);

  const onReject = useCallback((applyInfo) => {
    if (!applyInfo) {
      return;
    }
    setApplyList((applyList) => applyList.filter((v) => v.id != applyInfo.id));
  }, []);

  const onApprove = useCallback((applyInfo) => {
    if (!applyInfo) {
      return;
    }
    // setApplyList((applyList) =>
    //   applyList.map((v) => (v.id == applyInfo.id ? { ...applyInfo } : v))
    // );
    setApplyList((applyList) => applyList.filter((v) => v.id != applyInfo.id));
    setRequestInfo((requestInfo) => ({ ...requestInfo, state: 20 }));
  }, []);

  const onClickOffer = useCallback(() => {
    if (!userInfo.isHelper) {
      if (
        window.confirm(
          '제안하기는 헬퍼로 등록된 사용자만 가능합니다.\n헬퍼등록 신청 화면으로 이동하시겠습니까?'
        )
      ) {
        navigateScroll('/helper');
      }
      return;
    }
    setOpenOfferModal(true);
  }, [userInfo]);

  const navigateScroll = useCallback(
    (to) => {
      navigate(to);
      window.scroll({
        top: 0,
        behavior: 'smooth',
      });
    },
    [navigate]
  );

  return (
    <div className='View_root'>
      <article className='View_story-content'>
        <section className='View_article-profile'>
          <div className='View_title-box'>
            <span className='View_state'>{state}</span>
            <span className='View_title'>{requestInfo?.title}</span>
          </div>
          <div className='View_profile'>
            <div className='View_profile-image-box'>
              <div className='View_rounding-profile-image'>
                <img src='' onError={onFallbackImg} />
              </div>
            </div>
            <div className='View_profile-detail-box'>
              <div className='View_profile-name-box'>
                <span className='View_profile-name'>
                  {requestInfo?.writerInfo?.nickname}
                </span>
                {!userInfo?.isAdmin && !isMine && (
                  <FontAwesomeIcon
                    icon={faMessage}
                    className='View_message-icon'
                    onClick={onClickMessage}
                  />
                )}
              </div>
              <div className='View_address-box'>
                <span className='View_address'>{requestInfo?.address1}</span>
                <LocationOnIcon className='View_location-icon' />
              </div>
            </div>
            <div className='View_profile-right'>
              <dl className='View_temperature-wrap'>
                <dt className='View_trust-point'>신뢰도</dt>
                <dd className='View_percent'>
                  {Math.round(requestInfo?.writerInfo?.trustPoint * 10) / 10.0}
                </dd>
              </dl>
            </div>
          </div>
          <div
            className={'View_map' + (requestInfo ? '' : ' View_map_hidden')}
          ></div>
        </section>

        <section className='View_condition-section'>
          <div className='View_condition'>
            <div className='View_condition-left'>
              {requestInfo?.isCanSuggestPayment ? (
                <Chip
                  className='View_suggest-O'
                  label='초과 제안 가능'
                  color='primary'
                  variant='outlined'
                />
              ) : (
                <Chip
                  className='View_suggest-N'
                  label='초과 제안 불가'
                  color='primary'
                  variant='outlined'
                />
              )}

              <Chip
                className='View_payment'
                label={'₩ ' + requestInfo?.payment.toLocaleString('ko-KR')}
                color='primary'
                variant='contained'
              />
              {requestInfo?.deadline ? (
                <Chip
                  className='View_deadLine'
                  label={moment(requestInfo.deadline).format(
                    'YYYY/MM/DD' + ' 까지'
                  )}
                  color='primary'
                  variant='contained'
                />
              ) : (
                <Chip
                  className='View_deadLine'
                  label='만료기한 없음'
                  color='primary'
                  variant='contained'
                />
              )}
              <Chip
                className='View_category'
                label={category}
                color='primary'
                variant='contained'
              />
            </div>
            <div className='View_condition-right'>
              <IconButton
                aria-label='more'
                className='View_long-button'
                aria-controls={open ? 'long-menu' : undefined}
                aria-expanded={open ? 'true' : undefined}
                aria-haspopup='ture'
                onClick={handleClick}
              >
                <MoreVertIcon />
              </IconButton>
              <Menu
                MenuListProps={{ 'aria-labelledby': 'long-button' }}
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
              >
                {userInfo?.isAdmin ? (
                  <div>
                    <MenuItem onClick={onClickBlock}>불량글 제재하기</MenuItem>
                    <MenuItem onClick={onClickHide}>
                      신고 목록에서 감추기
                    </MenuItem>
                  </div>
                ) : isMine ? (
                  <div>
                    <MenuItem onClick={onClickRefresh}>끌어올리기</MenuItem>
                    {requestInfo?.state == 10 && (
                      <MenuItem onClick={onClickComplete}>완료로 변경</MenuItem>
                    )}
                    {requestInfo?.state == 30 && (
                      <MenuItem onClick={onClickWaiting}>
                        모집중으로 변경
                      </MenuItem>
                    )}
                    <MenuItem onClick={onClickModify}>수정하기</MenuItem>
                    <MenuItem onClick={onClickDelete}>삭제하기</MenuItem>
                  </div>
                ) : (
                  <div>
                    <MenuItem onClick={onClickOffer}>제안하기</MenuItem>
                    <MenuItem onClick={onClickReport}>신고하기</MenuItem>
                  </div>
                )}
              </Menu>
            </div>
          </div>
        </section>

        <section className='View_article-description'>
          <div
            className='View_article-detail'
            dangerouslySetInnerHTML={{ __html: requestInfo?.desscription }}
          ></div>
          <div className='View_count-box'>
            <time className='reg_time'>
              {requestInfo?.modTime
                ? `${timeForToday(requestInfo?.modTime)} 수정됨`
                : timeForToday(requestInfo?.regTime)}
            </time>{' '}
            ∙ 조회
            {' ' + requestInfo?.viewCount}
          </div>
        </section>
        <span className='View_income-offers'>들어온 제안들</span>
        <div className='View_income-offers-list'>
          {applyList.map((v) => (
            <ViewOffer
              applyInfo={v}
              key={v.id}
              onDelete={onDelete}
              onConfirm={onConfirm}
              onReject={onReject}
              onApprove={onApprove}
            />
          ))}
          <div ref={bottomRef} className='View_bottom'>
            {isLoading && <Loader />}
          </div>
        </div>
        <section className='View_control-buttons'>
          {!userInfo?.isAdmin && !isMine && (
            <Button
              className='View_write'
              variant='contained'
              onClick={onClickOffer}
            >
              <DriveFileRenameOutline />
              제안하기
            </Button>
          )}
          <Link to='/'>
            <Button className='View_home' variant='contained' sx={{ mt: 0 }}>
              목록
            </Button>
          </Link>
          <View_OfferForm
            open={openOfferModal}
            onConfirm={onConfirm}
            onClose={() => setOpenOfferModal(false)}
            requestId={requestInfo?.requestId}
          />
        </section>
      </article>
    </div>
  );
}
