import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import Rating from '@mui/material/Rating';
import StarIcon from '@mui/icons-material/Star';
import TextareaAutosize from '@mui/material/TextareaAutosize';
import ClearIcon from '@mui/icons-material/Clear';
import React, { useCallback } from 'react';
import './Comment.css';
import { writeReview } from '../../axios/axios_actions';

const labels = {
  0: '최악이에요',
  1: '나빠요',
  2: '별로예요',
  3: '보통이예요',
  4: '좋아요',
  5: '최고예요!',
};

const style = {
  top: '50%',
  left: '50%',
  transoform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 7,
};

export default function Comment({
  orderId,
  open,
  handleClose,
  targetInfo,
  onConfrim,
}) {
  const [value, setValue] = React.useState(3);
  const [hover, setHover] = React.useState(-1);
  const [description, setDescription] = React.useState('');

  const onChangeDesc = useCallback(({ target }) => {
    setDescription(target.value);
  }, []);

  const onClickConfirm = useCallback(() => {
    if (
      !window.confirm(
        '이대로 후기를 작성하시겠습니까?\n작성된 후기는 변경할 수 없습니다.'
      )
    ) {
      return;
    }

    writeReview(orderId, description, value)
      .then(({ data }) => {
        alert('후기를 작성했습니다.');
        onConfrim?.(data);
        handleClose?.();
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [value, description, orderId, handleClose, onConfrim]);

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby='modal-title'
        aria-describedby='modal-description'
      >
        <Box sx={style} className='Comment_wrap'>
          <ClearIcon className='Comment_exit' onClick={handleClose} />
          <Typography
            className='comment_title'
            variant='h6'
            component='h2'
            sx={{ mb: 2 }}
          >
            {targetInfo?.nickname}님과
            <br />
            거래가 어떠셨나요?
          </Typography>
          <Box
            sx={{ width: 400, display: 'flex', alignItems: 'center', mr: 10 }}
          >
            <Typography className='first_rating'></Typography>
            <Rating
              name='hover-feedback'
              value={value}
              precision={1}
              onChange={(event, newValue) => {
                if (newValue > 0) {
                  setValue(newValue);
                }
              }}
              onChangeActive={(event, newHover) => {
                setHover(newHover);
              }}
              emptyIcon={
                <StarIcon style={{ opacity: 0.55 }} fontSize='inherit' />
              }
            />
            {value !== null && (
              <Box sx={{ ml: 3, mt: 0.5 }}>
                {labels[hover !== -1 ? hover : value]}
              </Box>
            )}
          </Box>
          <TextareaAutosize
            minRows={10}
            className='Comment_content'
            placeholder='후기를 남겨주세요'
            value={description}
            onChange={onChangeDesc}
            style={{
              width: 300,
              height: 300,
              m: 1,
            }}
          />

          <Button
            className='Comment_completeBtn'
            variant='contained'
            sx={{ mt: 1 }}
            onClick={onClickConfirm}
          >
            완료
          </Button>
        </Box>
      </Modal>
    </div>
  );
}
