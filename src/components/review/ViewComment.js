import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import Rating from '@mui/material/Rating';
import StarIcon from '@mui/icons-material/Star';
import TextareaAutosize from '@mui/material/TextareaAutosize';
import ClearIcon from '@mui/icons-material/Clear';
import React from 'react';
import './ViewComment.css';

const labels = {
  0: '최악이에요',
  1: '나빠요',
  2: '별로예요',
  3: '보통이예요',
  4: '좋아요',
  5: '최고예요!',
};

const style = {
  top: '50%',
  left: '50%',
  transoform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 7,
};

export default function Comment({ reviewInfo, open, handleClose }) {
  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby='modal-modal-modal-title'
        aria-describedby='modal-modal-modal-description'
      >
        <Box sx={style} className='ViewComment_wrap'>
          <ClearIcon className='ViewComment_exit' onClick={handleClose} />
          <Typography
            className='ViewComment_title'
            variant='h6'
            component='h2'
            sx={{ mb: 2 }}
          >
            {reviewInfo?.writerInfo?.nickname}님의
            <br />
            후기를 확인해보세요!
          </Typography>
          <Box
            sx={{ width: 400, display: 'flex', alignItems: 'center', mr: 10 }}
          >
            <Rating
              name='text-feedback'
              value={reviewInfo?.evaluation}
              precision={1}
              readOnly
              emptyIcon={
                <StarIcon style={{ opacity: 0.55 }} fontSize='inherit' />
              }
            />
            {reviewInfo?.evaluation >= 0 && (
              <Box sx={{ ml: 3, mt: 0.5 }}>
                {labels[reviewInfo?.evaluation]}
              </Box>
            )}
          </Box>
          <TextareaAutosize
            minRows={10}
            className='ViewComment_content'
            placeholder={reviewInfo?.content}
            readOnly
            style={{
              width: 300,
              height: 300,
              m: 1,
            }}
          />

          <Button
            className='ViewComment_completeBtn'
            variant='contained'
            sx={{ mt: 1, width: 300, backgroundColor: '#6ab3c0' }}
            onClick={handleClose}
          >
            확인
          </Button>
        </Box>
      </Modal>
    </div>
  );
}
