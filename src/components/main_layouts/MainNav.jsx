import './MainNav.css';

import React, { useCallback, useState, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate, useLocation } from 'react-router-dom';
import { Navbar, Dropdown } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faSearch,
  faFilter,
  faUser,
  faBars,
} from '@fortawesome/free-solid-svg-icons';
import { openChatRoomList } from '../../_redux/actions/modal_action';
import { logout } from '../../_redux/actions/user_actions';
import SearchFilterModal from '../request/SearchFilterModal';

const UserInfoToggle = React.forwardRef(({ children, onClick }, ref) => (
  <div
    ref={ref}
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
  >
    {children}
  </div>
));

const MainNav = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { pathname } = useLocation();

  const isLoggedIn = useSelector((state) => state.user.isLoggedIn);
  const userInfo = useSelector((state) => state.user.userInfo);

  const [showFilter, setShowFilter] = useState(false);
  const [selectedCategories, setSelectedCategories] = useState(new Set());
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [keyword, setKeyword] = useState('');
  const [isShowDropdown, setIsShowDropdown] = useState(false);

  const onToggleHandler = useCallback((isOpen, e, metadata) => {
    setIsShowDropdown(isOpen);
  }, []);

  const openChatRoomListModal = useCallback((event) => {
    event.preventDefault();
    dispatch(openChatRoomList());
  }, []);

  const onClickAnchor = useCallback((event) => {
    event.preventDefault();
    navigate(event.target.getAttribute('href'));
  }, []);

  const onClickLogout = useCallback((event) => {
    event.preventDefault();

    dispatch(logout());
    setIsShowDropdown(false);
  }, []);

  const onClickFilter = useCallback(() => {
    setShowFilter(true);
  }, []);

  const toggleCategory = useCallback((code) => {
    setSelectedCategories((categories) => {
      if (categories.has(code)) {
        categories.delete(code);
      } else {
        categories.add(code);
      }
      return new Set([...categories]);
    });
  }, []);

  const onChangeKeyword = useCallback(({ target }) => {
    setKeyword(target.value);
  }, []);

  const onClickSearch = useCallback(() => {
    const searchParams = new URLSearchParams();
    selectedCategories.forEach((v) => {
      searchParams.append('c', v);
    });
    parseInt(minPrice)
      ? searchParams.set('mp', minPrice)
      : searchParams.delete('mp');
    parseInt(maxPrice)
      ? searchParams.set('xp', maxPrice)
      : searchParams.delete('xp');
    if (keyword.trim()) {
      searchParams.set('qs', keyword.trim());
    }
    navigate({
      pathname: '/',
      search: searchParams.toString(),
    });
    window.scroll({
      top: 0,
      behavior: 'smooth',
    });
  }, [selectedCategories, minPrice, maxPrice, keyword]);

  const onKeyUpSearch = useCallback(
    (event) => {
      if (event.keyCode == 13) {
        event.preventDefault();
        onClickSearch();
        return;
      }
    },
    [onClickSearch]
  );

  const closeFilter = useCallback(() => {
    setShowFilter(false);
    if (pathname == '/') {
      onClickSearch();
    }
  }, [pathname, onClickSearch]);

  const onClickBrand = useCallback((event) => {
    event.preventDefault();
    navigate('/');
  }, []);

  return (
    <Navbar fixed='top' className='MainNav_navbar'>
      <Navbar.Brand className='MainNav_brand' href='/' onClick={onClickBrand}>
        DOBDA
      </Navbar.Brand>
      <div className='ms-auto me-auto MainNav_search-bar'>
        <input
          type='text'
          placeholder='검색어를 입력해주세요.'
          onKeyUp={onKeyUpSearch}
          value={keyword}
          onChange={onChangeKeyword}
        />
        <div className='MainNav_search-btn-bar'>
          <button className='MainNav_filter-btn' onClick={onClickFilter}>
            <FontAwesomeIcon icon={faFilter} />
          </button>
          {showFilter && (
            <SearchFilterModal
              selectedCategories={selectedCategories}
              toggleCategory={toggleCategory}
              minPrice={minPrice}
              maxPrice={maxPrice}
              onChangeMinPrice={({ currentTarget }) =>
                setMinPrice(currentTarget.value)
              }
              onChangeMaxPrice={({ currentTarget }) =>
                setMaxPrice(currentTarget.value)
              }
              onClose={closeFilter}
              closeWhenClickBack
            />
          )}
          <div className='MainNav_vl' />
          <button className='MainNav_search-btn' onClick={onClickSearch}>
            <FontAwesomeIcon icon={faSearch} />
          </button>
        </div>
      </div>

      <Dropdown
        show={isShowDropdown}
        onToggle={(isOpen, e, metadata) => onToggleHandler(isOpen, e, metadata)}
      >
        <Dropdown.Toggle as={UserInfoToggle} id='dropdown-custom-components'>
          <div className='MainNav_right-menu-bar'>
            <button className='MainNav_menu-btn'>
              <FontAwesomeIcon icon={faBars} />
            </button>
            <div className='MainNav_user-info-btn-box'>
              <button className='MainNav_user-info-btn'>
                <FontAwesomeIcon icon={faUser} />
                {(userInfo?.unreadNotiCount > 0 || userInfo?.hasUnreadChat) && (
                  <div className='MainNav_unread'></div>
                )}
              </button>
            </div>
          </div>
        </Dropdown.Toggle>
        <Dropdown.Menu>
          {isLoggedIn ? (
            <>
              {userInfo?.isAdmin ? (
                <>
                  <Dropdown.Item href='/admin/myPage' onClick={onClickAnchor}>
                    관리자 페이지
                  </Dropdown.Item>
                </>
              ) : (
                <>
                  <Dropdown.Item href='/myPage' onClick={onClickAnchor}>
                    마이 페이지
                  </Dropdown.Item>
                  <Dropdown.Item href='/myPage/noti' onClick={onClickAnchor}>
                    알림{userInfo?.unreadNotiCount > 0 && '*'}
                  </Dropdown.Item>
                  <Dropdown.Item onClick={openChatRoomListModal}>
                    메시지{userInfo?.hasUnreadChat && '*'}
                  </Dropdown.Item>
                </>
              )}
              <Dropdown.Divider />
              <Dropdown.Item onClick={onClickLogout}>로그아웃</Dropdown.Item>
            </>
          ) : (
            <>
              <Dropdown.Item href='/signIn' onClick={onClickAnchor}>
                로그인
              </Dropdown.Item>
              <Dropdown.Item href='/signUp' onClick={onClickAnchor}>
                회원가입
              </Dropdown.Item>
            </>
          )}
        </Dropdown.Menu>
      </Dropdown>
    </Navbar>
  );
};

export default MainNav;
