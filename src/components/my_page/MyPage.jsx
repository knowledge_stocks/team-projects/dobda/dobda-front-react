import './MyPage.css';
import React from 'react';
import { Outlet, Link, useLocation } from 'react-router-dom';
import MyInfo from './MyInfo';

function MyPage() {
  const { pathname } = useLocation();

  return (
    <div className='MyPage_root'>
      <div className='MyPage_top-bar'>
        <div className='MyPage_menu-root'>
          <div className='MyPage_menu'>
            <div className='MyPage_menu-underline' />
            <Link
              to='myRequest'
              className={
                'MyPage_menu-button' +
                (pathname == '/myPage' || pathname == '/myPage/myRequest'
                  ? ' MyPage_menu-button-selected'
                  : '')
              }
            >
              <div className='MyPage_menu-button-underline' />
              <span>내 의뢰글</span>
            </Link>
            <Link
              to='myOrder'
              className={
                'MyPage_menu-button' +
                (pathname == '/myPage/myOrder'
                  ? ' MyPage_menu-button-selected'
                  : '')
              }
            >
              <div className='MyPage_menu-button-underline' />
              <span>맡긴 거래</span>
            </Link>
            <Link
              to='myAceptOffer'
              className={
                'MyPage_menu-button' +
                (pathname == '/myPage/myAceptOffer'
                  ? ' MyPage_menu-button-selected'
                  : '')
              }
            >
              <div className='MyPage_menu-button-underline' />
              <span>받은 제안</span>
            </Link>
            <div className='MyPage_seperator'></div>
            <Link
              to='myAceptOrder'
              className={
                'MyPage_menu-button' +
                (pathname == '/myPage/myAceptOrder'
                  ? ' MyPage_menu-button-selected'
                  : '')
              }
            >
              <div className='MyPage_menu-button-underline' />
              <span>맡은 거래</span>
            </Link>
            <Link
              to='myOffer'
              className={
                'MyPage_menu-button' +
                (pathname == '/myPage/myOffer'
                  ? ' MyPage_menu-button-selected'
                  : '')
              }
            >
              <div className='MyPage_menu-button-underline' />
              <span>나의 제안</span>
            </Link>
            <div className='MyPage_seperator'></div>
            <Link
              to='noti'
              className={
                'MyPage_menu-button' +
                (pathname == '/myPage/noti'
                  ? ' MyPage_menu-button-selected'
                  : '')
              }
            >
              <div className='MyPage_menu-button-underline' />
              <span>알림</span>
            </Link>
            <Link
              to='myInfo'
              className={
                'MyPage_menu-button' +
                (pathname == '/myPage/myInfo'
                  ? ' MyPage_menu-button-selected'
                  : '')
              }
            >
              <div className='MyPage_menu-button-underline' />
              <span>내 정보</span>
            </Link>
          </div>
        </div>
      </div>

      <div className='MyPage_content'>{Outlet ? <Outlet /> : <MyInfo />}</div>
    </div>
  );
}

export default MyPage;
