import './MyAcceptOffer.css';
import React, { useEffect, useState, useCallback } from 'react';
import RequestApplyItem from '../request_apply/RequestApplyItem';
import Loader from '../common/Loader';
import { useInView } from 'react-intersection-observer';
import { getMyAcceptOfferList } from '../../axios/axios_actions';

function MyAcceptOffer() {
  const [applyList, setApplyList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(0);
  const [isEnd, setIsEnd] = useState(false);
  const [bottomRef, inView] = useInView();

  useEffect(async () => {
    if (page === 0) {
      return;
    }
    setIsLoading(true);
    try {
      const response = await getMyAcceptOfferList(page);
      setApplyList((applyList) => [...applyList, ...response.data.content]);
      setIsEnd(response.data.last);
    } catch {
      setPage((prevState) => prevState - 1);
    }
    await new Promise((resolve) => setTimeout(resolve, 500));
    setIsLoading(false);
  }, [page]);

  useEffect(() => {
    if (inView && !isLoading && !isEnd) {
      setPage((prevState) => prevState + 1);
    }
  }, [inView, isLoading, isEnd]);

  const onReject = useCallback((applyInfo) => {
    if (!applyInfo) {
      return;
    }
    setApplyList((applyList) => applyList.filter((v) => v.id != applyInfo.id));
  }, []);

  const onApprove = useCallback((applyInfo) => {
    if (!applyInfo) {
      return;
    }
    setApplyList((applyList) => applyList.filter((v) => v.id != applyInfo.id));
  }, []);

  return (
    <div className='MyAcceptOffer_root'>
      <div className='MyAcceptOffer_list'>
        {applyList.map((v, idx) => (
          <RequestApplyItem
            applyInfo={v}
            key={idx}
            onReject={onReject}
            onApprove={onApprove}
            isHelperProfile
          />
        ))}
        <div ref={bottomRef} className='MyAcceptOffer_bottom'>
          {isLoading && <Loader />}
        </div>
      </div>
    </div>
  );
}

export default MyAcceptOffer;
