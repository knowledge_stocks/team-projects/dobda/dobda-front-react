import './MyApply.css';
import React, { useEffect, useState, useCallback } from 'react';
import RequestApplyItem from '../request_apply/RequestApplyItem';
import Loader from '../common/Loader';
import { useInView } from 'react-intersection-observer';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import {
  getMyReqApplyList,
  getRegisterHelperState,
  cancelRegisterHelper,
} from '../../axios/axios_actions';
import moment from 'moment-timezone';

function MyApply() {
  const userInfo = useSelector((state) => state.user.userInfo);

  const [filterType, setFilterType] = useState(10);
  const [applyList, setApplyList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(0);
  const [isEnd, setIsEnd] = useState(false);
  const [helperState, setHelperState] = useState();
  const [isLoaded, setIsLoaded] = useState(false);
  const [bottomRef, inView] = useInView();

  useEffect(() => {
    if (userInfo?.isHelper) {
      setIsLoaded(true);
      return;
    }
    getRegisterHelperState().then((response) => {
      setHelperState(response.data);
      setIsLoaded(true);
    });
  }, [userInfo]);

  useEffect(() => {
    setApplyList([]);
    setIsEnd(false);
    setPage(0);
  }, [filterType]);

  useEffect(async () => {
    if (page === 0) {
      return;
    }
    setIsLoading(true);
    try {
      const response = await getMyReqApplyList(page, [filterType]);
      setApplyList((applyList) => [...applyList, ...response.data.content]);
      setIsEnd(response.data.last);
    } catch {
      setPage((prevState) => prevState - 1);
    }
    await new Promise((resolve) => setTimeout(resolve, 500));
    setIsLoading(false);
  }, [page, filterType]);

  useEffect(() => {
    if (inView && !isLoading && !isEnd) {
      setPage((prevState) => prevState + 1);
    }
  }, [inView, isLoading, isEnd]);

  const onSelectedFilterType = useCallback(
    (event) => {
      event.preventDefault();
      if (isLoading) {
        return;
      }
      setFilterType(event.currentTarget.dataset.type);
    },
    [isLoading]
  );

  const onDelete = useCallback((applyInfo) => {
    if (!applyInfo) {
      return;
    }
    setApplyList((applyList) => applyList.filter((v) => v.id != applyInfo.id));
  }, []);

  const onConfirm = useCallback((applyInfo) => {
    if (!applyInfo) {
      return;
    }
    setApplyList((applyList) => {
      const newList = applyList.filter((v) => v.id != applyInfo.id);
      newList.unshift(applyInfo);
      return newList;
    });
  }, []);

  const onClickCancelRegister = useCallback(() => {
    if (!window.confirm('정말로 헬퍼 신청을 취소하실거에요? ㅠㅠ')) {
      return;
    }

    cancelRegisterHelper()
      .then(() => {
        alert('헬퍼 신청이 취소되었습니다.');
        setHelperState();
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, []);

  return (
    <div className='MyApply_root'>
      {userInfo?.isHelper ? (
        <>
          <div className='MyApply_menu-root'>
            <div className='MyApply_menu'>
              <div className='MyApply_menu-underline' />
              <a
                onClick={onSelectedFilterType}
                data-type='10'
                className={
                  'MyApply_menu-button' +
                  (filterType == 10 ? ' MyApply_menu-button-selected' : '')
                }
              >
                <div className='MyApply_menu-button-underline' />
                <span>대기중</span>
              </a>
              <a
                onClick={onSelectedFilterType}
                data-type='30'
                className={
                  'MyApply_menu-button' +
                  (filterType == 30 ? ' MyApply_menu-button-selected' : '')
                }
              >
                <div className='MyApply_menu-button-underline' />
                <span>수락됨</span>
              </a>
              <a
                onClick={onSelectedFilterType}
                data-type='20'
                className={
                  'MyApply_menu-button' +
                  (filterType == 20 ? ' MyApply_menu-button-selected' : '')
                }
              >
                <div className='MyApply_menu-button-underline' />
                <span>거절됨</span>
              </a>
            </div>
          </div>
          <div className='MyApply_list'>
            {applyList.map((v, idx) => (
              <RequestApplyItem
                applyInfo={v}
                key={idx}
                onDelete={onDelete}
                onConfirm={onConfirm}
              />
            ))}
            <div ref={bottomRef} className='MyApply_bottom'>
              {isLoading && <Loader />}
            </div>
          </div>
        </>
      ) : (
        <div className='MyApply_registerHelper_root'>
          {helperState?.state == 10 ? (
            <>
              <p>
                {moment(helperState?.regTime).format('YYYY년 MM월 DD일')}에
                신청한 헬퍼 신청이 아직 심사중입니다.
              </p>
              <Link to='/helper'>
                <Button variant='secondary'>다시 신청하기</Button>
              </Link>
              <Button
                style={{ marginLeft: '10px' }}
                variant='danger'
                onClick={onClickCancelRegister}
              >
                신청 취소하기
              </Button>
            </>
          ) : helperState?.state == 20 ? (
            <>
              <p>
                {moment(helperState.regTime).format('YYYY년 MM월 DD일')}에
                신청한 헬퍼 신청이 거부되었습니다.
              </p>
              <Link to='/helper'>
                <Button variant='secondary'>다시 신청하기</Button>
              </Link>
            </>
          ) : (
            isLoaded && (
              <>
                <p>아직 헬퍼가 아니에요.</p>
                <p>지금 바로 헬퍼로 등록해보세요.</p>
                <Link to='/helper'>
                  <Button>헬퍼 등록 신청하러 가기</Button>
                </Link>
              </>
            )
          )}
        </div>
      )}
    </div>
  );
}

export default MyApply;
