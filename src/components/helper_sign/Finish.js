import { IconButton, Typography } from '@mui/material';
import React, { useCallback } from 'react';
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import Button from '@mui/material/Button';
import './Finish.css';

export default function Finish({ image, setImage }) {
  const onImageSeleted = useCallback((e) => {
    e.preventDefault();
    if (e.target.files.length == 0) {
      return;
    }

    const img = e.target.files[0];
    document.querySelector('#uldri1').files = new DataTransfer().files;

    const reader = new FileReader();
    reader.readAsDataURL(img);
    reader.onloadend = () => {
      setImage({ file: img, url: reader.result });
    };
  }, []);
  const resetImages = useCallback((e) => {
    e.preventDefault();
    setImage();
  }, []);

  return (
    <div className='Finish_root'>
      <form>
        <Typography id='finish_photoUpload' variant='h6' gutterBottom>
          신분증 등록
        </Typography>
        <div className='Finish_image-box'>
          {!image && (
            <label htmlFor='uldri1'>
              <input
                type='file'
                id='uldri1'
                className='uldri'
                accept='image/*'
                onChange={onImageSeleted}
              />
              <IconButton
                id='helper_upload'
                color='primary'
                aria-label='upload picture'
                component='span'
              >
                <PhotoCamera />
              </IconButton>
            </label>
          )}
          {image?.url && (
            <img className='Finish_img' src={image?.url} alt='신분증' />
          )}
        </div>
        <Button variant='contained' id='finish_Reset' onClick={resetImages}>
          리셋
        </Button>
      </form>
    </div>
  );
}
