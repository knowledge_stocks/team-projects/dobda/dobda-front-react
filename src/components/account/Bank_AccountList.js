import React, { useState, useEffect } from 'react';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import ClearIcon from '@mui/icons-material/Clear';
import { getMyAccountList } from '../../axios/axios_actions';

import './Bank_AccountList.css';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 500,
  bgcolor: 'background.paper',
  border: '0px',
  boxShadow: 24,
  p: 2.5,
};

export default function Bank_AccountList({ open, onClose }) {
  const [accountList, setAccountList] = useState([]);
  const [isTokenInvalid, setIsTokenInvalid] = useState(false);
  const [bankAuthPopup, setBankAuthPopup] = useState();
  const [bankAuthCode, setBankAuthCode] = useState();

  useEffect(() => {
    if (!open) {
      return;
    }
    getMyAccountList()
      .then((response) => {
        setAccountList(response.data.accounts);
      })
      .catch((err) => {
        if (err.response?.status == 401) {
          const width = '500';
          const height = '600';
          const left = Math.ceil((window.screen.width - width) / 2);
          const top = Math.ceil((window.screen.height - height) / 2);
          const options = `top=${top}, left=${left}, width=${width}, height=${height}, status=no, menubar=no, toolbar=no, resizable=no, location=no, directories=no`;
          const popup = window.open(
            `${process.env.REACT_APP_OPEN_BANK_HOST}/oauth/2.0/authorize` +
              '?response_type=code' +
              `&client_id=${process.env.REACT_APP_OPEN_BANK_CLIENT_ID}` +
              `&redirect_uri=${process.env.REACT_APP_OPEN_BANK_REDIRECT}` +
              '&scope=login+transfer' +
              '&state=b80BLsfigm9OokPTjy03elbJqRHOfGSY' +
              '&auth_type=0',
            null,
            options
          );
          var timer = setInterval(function () {
            if (popup.closed) {
              clearInterval(timer);
              setBankAuthPopup();
            } else if (popup?.location?.pathname == '/openbank/auth') {
              setBankAuthCode(
                new URLSearchParams(popup.location.search).get('code')
              );
              popup.close();
            }
          }, 100);
          setBankAuthPopup(popup);
          return;
        } else if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
        onClose?.();
      });
  }, [open]);

  return (
    <Modal
      open={open}
      onClose={onClose}
      aria-labelledby='modal-modal-title'
      aria-describedby='modal-modal-description'
    >
      <Box className='Bank_AccountList_Form' sx={style}>
        <div className='Bank_AccountList_top-bar'>
          <ClearIcon className='Bank_AccountList_exit' onClick={onClose} />
        </div>
        {isTokenInvalid ? (
          <div className='Bank_AccountList_list'>
            계좌 인증 정보가 만료되었거나 유효하지 않습니다.
            <br />
            창을 닫지 마시고 인증을 완료해주세요.
          </div>
        ) : (
          <div className='Bank_AccountList_list'>
            {accountList.map((v) => (
              <ListItem component='div' disablePadding>
                <ListItemButton>
                  <ListItemText
                    primary={`${v.bankName} / ${v.accountNumMasked} / ${v.accountHolderName}`}
                  />
                </ListItemButton>
              </ListItem>
            ))}
          </div>
        )}
      </Box>
    </Modal>
  );
}
