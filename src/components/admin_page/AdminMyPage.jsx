import './AdminMyPage.css';
import React from 'react';
import { Outlet, Link, useLocation } from 'react-router-dom';
import AdminMyInfo from './AdminMyInfo';

function AdminMyPage() {
  const { pathname } = useLocation();

  return (
    <div className='AdminMyPage_root'>
      <div className='AdminMyPage_top-bar'>
        <div className='AdminMyPage_menu-root'>
          <div className='AdminMyPage_menu'>
            <div className='AdminMyPage_menu-underline' />
            <Link
              to='request'
              className={
                'AdminMyPage_menu-button' +
                (pathname == '/admin/myPage/request'
                  ? ' AdminMyPage_menu-button-selected'
                  : '')
              }
            >
              <div className='AdminMyPage_menu-button-underline' />
              <span>불량글 관리</span>
            </Link>
            <Link
              to='helper'
              className={
                'AdminMyPage_menu-button' +
                (pathname == '/admin/myPage/helper'
                  ? ' AdminMyPage_menu-button-selected'
                  : '')
              }
            >
              <div className='AdminMyPage_menu-button-underline' />
              <span>헬퍼 신청 관리</span>
            </Link>
            <div className='AdminMyPage_seperator'></div>
            <Link
              to='myInfo'
              className={
                'AdminMyPage_menu-button' +
                (pathname == '/admin/myPage' ||
                pathname == '/admin/myPage/myInfo'
                  ? ' AdminMyPage_menu-button-selected'
                  : '')
              }
            >
              <div className='AdminMyPage_menu-button-underline' />
              <span>내 정보</span>
            </Link>
          </div>
        </div>
      </div>

      <div className='AdminMyPage_content'>
        {Outlet ? <Outlet /> : <AdminMyInfo />}
      </div>
    </div>
  );
}

export default AdminMyPage;
