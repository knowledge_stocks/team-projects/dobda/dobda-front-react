import './ReportedRequest.css';
import React, { useEffect, useState, useCallback } from 'react';
import RequestItem from '../request/RequestItem';
import Loader from '../common/Loader';
import { useInView } from 'react-intersection-observer';
import { getReportedRequestList } from '../../axios/axios_actions';

function ReportedRequest() {
  const [requestList, setRequestList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(0);
  const [isEnd, setIsEnd] = useState(false);
  const [bottomRef, inView] = useInView();

  useEffect(async () => {
    if (page === 0) {
      return;
    }
    setIsLoading(true);
    try {
      const response = await getReportedRequestList(page);
      setRequestList((requestList) => [
        ...requestList,
        ...response.data.content,
      ]);
      setIsEnd(response.data.last);
    } catch {
      setPage((prevState) => prevState - 1);
    }
    await new Promise((resolve) => setTimeout(resolve, 500));
    setIsLoading(false);
  }, [page]);

  useEffect(() => {
    if (inView && !isLoading && !isEnd) {
      setPage((prevState) => prevState + 1);
    }
  }, [inView, isLoading, isEnd]);

  const onHide = useCallback((requestInfo) => {
    if (!requestInfo) {
      return;
    }
    setRequestList((requestList) =>
      requestList.filter((v) => v.requestId != requestInfo.requestId)
    );
  }, []);

  const onBlock = useCallback((requestInfo) => {
    if (!requestInfo) {
      return;
    }
    setRequestList((requestList) =>
      requestList.filter((v) => v.requestId != requestInfo.requestId)
    );
  }, []);

  return (
    <div className='ReportedRequest_root'>
      <div className='ReportedRequest_list'>
        {requestList.map((v, idx) => (
          <RequestItem
            requestInfo={v}
            key={idx}
            onBlock={onBlock}
            onHide={onHide}
          />
        ))}
        <div ref={bottomRef} className='ReportedRequest_bottom'>
          {isLoading && <Loader />}
        </div>
      </div>
    </div>
  );
}

export default ReportedRequest;
