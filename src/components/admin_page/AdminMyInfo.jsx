import './AdminMyInfo.css';
import React, { useState, useCallback, useMemo, useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { myInfo } from '../../_redux/actions/user_actions';
import UnderlineInput from '../common/UnderlineInput';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button } from 'react-bootstrap';
import {
  faPenToSquare,
  faCircleCheck,
  faCircleXmark,
} from '@fortawesome/free-solid-svg-icons';
import {
  changeEmailAdmin,
  changePwdAdmin,
  changePhoneNumAdmin,
  changeNicknameAdmin,
  leaveAdmin,
} from '../../axios/axios_actions';
import { logout } from '../../_redux/actions/user_actions';

function AdminMyInfo() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const userInfo = useSelector((state) => state.user.userInfo);

  const [email, setEmail] = useState('');
  const [pwd, setPwd] = useState('');
  const [modPwd, setModPwd] = useState('');
  const [nickname, setNickname] = useState('');
  const [phoneNum, setPhoneNum] = useState('');
  const [phonePwd, setPhonePwd] = useState('');
  const [isModEamil, setIsModEamil] = useState(false);
  const [isModPwd, setIsModPwd] = useState(false);
  const [isModNickname, setIsModNickname] = useState(false);
  const [isModPhoneNum, setIsModPhoneNum] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);

  const delpwdinput = useRef();

  const [name] = useMemo(() => {
    if (!userInfo) {
      return [];
    }
    setEmail(userInfo.email);
    setNickname(userInfo.nickname);
    setPhoneNum(userInfo.phoneNum);
    return [userInfo.name];
  }, [userInfo]);

  const onChangeEmail = useCallback((e) => {
    setEmail(e.target.value);
  });

  const onChangePwd = useCallback((e) => {
    setPwd(e.target.value);
  });

  const onChangeModPwd = useCallback((e) => {
    setModPwd(e.target.value);
  });

  const onChangeNickname = useCallback((e) => {
    setNickname(e.target.value);
  });

  const onChangePhoneNum = useCallback((e) => {
    setPhoneNum(e.target.value);
  });

  const onChangePhonePwd = useCallback((e) => {
    setPhonePwd(e.target.value);
  });

  const cancelEditEmail = useCallback(() => {
    setEmail(userInfo.email);
    setIsModEamil(false);
  }, [userInfo]);

  const confirmEditEmail = useCallback(() => {
    if (userInfo?.email == email || !email) {
      alert('변경할 이메일을 입력해주세요.😥');
      return;
    }

    if (!window.confirm('정말로 이메일을 변경하시겠습니까?')) {
      return;
    }

    changeEmailAdmin(email)
      .then(() => {
        alert('이메일이 변경되었습니다.😊');
        setIsModEamil(false);
        dispatch(myInfo());
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [userInfo, email]);

  const cancelEditPwd = useCallback(() => {
    setPwd('');
    setModPwd('');
    setIsModPwd(false);
  }, [userInfo]);

  const cancelEditNickname = useCallback(() => {
    setNickname(userInfo.nickname);
    setIsModNickname(false);
  }, [userInfo]);

  const onClickConfirmPwd = useCallback(() => {
    if (!pwd || !modPwd) {
      alert('비밀번호를 입력해주세요.😥');
      return;
    }

    if (!window.confirm('정말로 비밀번호를 변경하시겠습니까?')) {
      return;
    }

    changePwdAdmin(pwd, modPwd)
      .then(() => {
        alert('비밀번호가 변경되었습니다.😊');
        setPwd('');
        setModPwd('');
        setIsModPwd(false);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [pwd, modPwd]);

  const confirmEditNickname = useCallback(() => {
    if (userInfo?.nickname == nickname || !nickname) {
      alert('변경할 닉네임을 입력해주세요.😥');
      return;
    }

    if (!window.confirm('정말로 닉네임을 변경하시겠습니까?')) {
      return;
    }

    changeNicknameAdmin(nickname)
      .then(() => {
        alert('닉네임이 변경되었습니다.😊');
        setIsModNickname(false);
        dispatch(myInfo());
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [nickname, userInfo]);

  const cancelEditPhoneNum = useCallback(() => {
    setPhoneNum(userInfo.phoneNum);
    setIsModPhoneNum(false);
    setPhonePwd('');
  }, [userInfo]);

  const onClickChangePhoneNum = useCallback(() => {
    if (phonePwd == '') {
      return alert('비밀번호를 입력해주세요.😥');
    }

    changePhoneNumAdmin(phoneNum, phonePwd)
      .then(() => {
        alert('전화번호가 변경되었습니다.😊');
        cancelEditPhoneNum();
        dispatch(myInfo());
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [phoneNum, phonePwd, cancelEditPhoneNum]);

  const deleteInProgress = (e) => {
    if (delpwdinput.current.value === '') {
      alert('비밀번호를 입력해주세요.');
      return;
    }

    leaveAdmin(delpwdinput.current.value)
      .then(() => {
        dispatch(logout());
        navigate('/');
        setShowDeleteModal(false);
      })
      .catch((err) => {
        if (err.response && err.response.status == 401) {
          alert('비밀번호가 일치하지 않습니다.');
        } else if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  };

  return (
    <div className='AdminMyInfo_root'>
      <div>
        <span className='AdminMyInfo_title'>개인정보</span>
      </div>
      <div>
        <div className='AdminMyInfo_subtitle'>
          <span>이메일</span>
          {isModEamil ? (
            <>
              <div className='AdminMyInfo_subtitle-button'>
                <FontAwesomeIcon
                  icon={faCircleCheck}
                  onClick={confirmEditEmail}
                />
              </div>
              <div
                className='AdminMyInfo_subtitle-button'
                onClick={cancelEditEmail}
              >
                <FontAwesomeIcon icon={faCircleXmark} />
              </div>
            </>
          ) : (
            <div
              className='AdminMyInfo_subtitle-button'
              onClick={() => {
                setIsModEamil(true);
              }}
            >
              <FontAwesomeIcon icon={faPenToSquare} />
            </div>
          )}
        </div>
        <div>
          <UnderlineInput
            value={email}
            onChange={onChangeEmail}
            readOnly={!isModEamil}
          />
        </div>
      </div>
      <div>
        <div className='AdminMyInfo_subtitle'>
          <span>비밀번호 변경</span>
          {isModPwd ? (
            <>
              <div className='AdminMyInfo_subtitle-button'>
                <FontAwesomeIcon
                  icon={faCircleCheck}
                  onClick={onClickConfirmPwd}
                />
              </div>
              <div
                className='AdminMyInfo_subtitle-button'
                onClick={cancelEditPwd}
              >
                <FontAwesomeIcon icon={faCircleXmark} />
              </div>
            </>
          ) : (
            <div
              className='AdminMyInfo_subtitle-button'
              onClick={() => {
                setIsModPwd(true);
              }}
            >
              <FontAwesomeIcon icon={faPenToSquare} />
            </div>
          )}
        </div>
        {isModPwd ? (
          <>
            <div>
              <UnderlineInput
                type='password'
                value={pwd}
                onChange={onChangePwd}
                placeholder='기존 비밀번호를 입력해주세요.'
              />
            </div>
            <div style={{ marginTop: '15px' }}>
              <UnderlineInput
                type='password'
                value={modPwd}
                placeholder='변경할 비밀번호를 입력해주세요.'
                onChange={onChangeModPwd}
              />
            </div>
          </>
        ) : null}
      </div>
      <div>
        <div className='AdminMyInfo_subtitle'>
          <span>실명</span>
        </div>
        <div>
          <UnderlineInput value={name} readOnly />
        </div>
      </div>
      <div>
        <div className='AdminMyInfo_subtitle'>
          <span>닉네임</span>
          {isModNickname ? (
            <>
              <div className='AdminMyInfo_subtitle-button'>
                <FontAwesomeIcon
                  icon={faCircleCheck}
                  onClick={confirmEditNickname}
                />
              </div>
              <div
                className='AdminMyInfo_subtitle-button'
                onClick={cancelEditNickname}
              >
                <FontAwesomeIcon icon={faCircleXmark} />
              </div>
            </>
          ) : (
            <div
              className='AdminMyInfo_subtitle-button'
              onClick={() => {
                setIsModNickname(true);
              }}
            >
              <FontAwesomeIcon icon={faPenToSquare} />
            </div>
          )}
        </div>
        <div>
          <UnderlineInput
            value={nickname}
            onChange={onChangeNickname}
            readOnly={!isModNickname}
          />
        </div>
      </div>
      <div>
        <div className='AdminMyInfo_subtitle'>
          <span>전화번호</span>
          {isModPhoneNum ? (
            <>
              <div
                className='AdminMyInfo_subtitle-button'
                onClick={cancelEditPhoneNum}
              >
                <FontAwesomeIcon icon={faCircleXmark} />
              </div>
            </>
          ) : (
            <div
              className='AdminMyInfo_subtitle-button'
              onClick={() => {
                setIsModPhoneNum(true);
              }}
            >
              <FontAwesomeIcon icon={faPenToSquare} />
            </div>
          )}
        </div>
        <div>
          <UnderlineInput
            value={phoneNum}
            onChange={onChangePhoneNum}
            readOnly={!isModPhoneNum}
          />
        </div>
        {isModPhoneNum && (
          <>
            <div className='AdminMyInfo_subtitle'>
              <span>비밀번호 확인</span>
              <div className='AdminMyInfo_subtitle-button'>
                <FontAwesomeIcon
                  icon={faCircleCheck}
                  onClick={onClickChangePhoneNum}
                />
              </div>
            </div>
            <div>
              <UnderlineInput
                value={phonePwd}
                type='password'
                onChange={onChangePhonePwd}
                placeholder='비밀번호를 입력해주세요'
                readOnly={false}
              />
            </div>
          </>
        )}
      </div>
      <div className='AdminMyInfo_leave-btn-box'>
        <Button variant='danger' onClick={() => setShowDeleteModal(true)}>
          회원 탈퇴
        </Button>
        {showDeleteModal ? (
          <div className='delete-modal'>
            <div className='ddmm01'>
              <p>
                탈퇴를 누르시면 모든 정보가 사라집니다.
                <br />
                탈퇴 하시겠습니까?
              </p>
              <input
                ref={delpwdinput}
                type='password'
                name='pwd'
                placeholder='비밀번호를 입력해주세요.'
              />
            </div>
            <div className='ddmm02'>
              <button
                className='ddmm02n'
                onClick={() => {
                  setShowDeleteModal(!showDeleteModal);
                }}
              >
                취소
              </button>
              <button className='ddmm02y' onClick={deleteInProgress}>
                탈퇴
              </button>
            </div>
          </div>
        ) : null}
      </div>
    </div>
  );
}

export default AdminMyInfo;
