import './UnderlineInput.css';
import React, { useRef, useCallback } from 'react';

function UnderlineInput({
  value,
  onChange = () => {},
  readOnly = false,
  placeholder,
  type = 'text',
}) {
  const rootRef = useRef();

  const onFocus = useCallback(() => {
    console.log();
    rootRef.current.classList.add('UnderlineInput_focus');
  });

  const onBlur = useCallback(() => {
    rootRef.current.classList.remove('UnderlineInput_focus');
  });

  return (
    <div>
      <div className='UnderlineInput_root' ref={rootRef}>
        <input
          type={type}
          value={value}
          onChange={onChange}
          onFocus={onFocus}
          onBlur={onBlur}
          readOnly={readOnly}
          placeholder={placeholder}
        ></input>
        <div className='UnderlineInput_underline' />
      </div>
    </div>
  );
}

export default UnderlineInput;
