import './PaymentModal.css';

import React, { useCallback, useEffect, useState } from 'react';
import { createPortal } from 'react-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import payMethodList from '../../types/payMethodList';
import { getMyAccountList } from '../../axios/axios_actions';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { approveRequestApply, reAuthBank } from '../../axios/axios_actions';

function PaymentModal({ applyId, closeWhenClickBack, onClose, onConfirm }) {
  const [selectedMethod, setSelectedMethod] = useState(payMethodList[0].code);
  const [accountList, setAccountList] = useState([]);
  const [selectedAccount, setSelectedAccount] = useState();
  const [isTokenInvalid, setIsTokenInvalid] = useState(false);
  const [bankAuthPopup, setBankAuthPopup] = useState();
  const [bankAuthCode, setBankAuthCode] = useState();
  const [password, setPassword] = useState('');

  useEffect(() => {
    document.body.style.overflow = 'hidden';
    return () => {
      document.body.style.overflow = 'unset';
      setBankAuthPopup((popup) => (popup ? popup.close() : undefined));
    };
  }, []);

  useEffect(() => {
    getMyAccountList()
      .then((response) => {
        setAccountList(response.data.accounts);
      })
      .catch((err) => {
        if (err.response?.status == 401) {
          setIsTokenInvalid(true);
          return;
        } else if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
        onClose?.();
      });
  }, []);

  const onClickBack = useCallback(
    (event) => {
      event.stopPropagation();
      closeWhenClickBack && onClose?.();
    },
    [closeWhenClickBack, onClose]
  );

  const onClickMethod = useCallback((code) => {
    setSelectedMethod(code);
  }, []);

  const onClickConfirm = useCallback(() => {
    if (selectedMethod == 2 && !selectedAccount) {
      alert('출금에 사용할 계좌를 선택해주세요.');
      return;
    }
    approveRequestApply(
      applyId,
      selectedMethod,
      selectedAccount?.accountHolderName,
      selectedAccount?.fintechNum
    )
      .then(() => {
        alert(
          '선택하신 제안이 수락되었습니다.\n주문 내역은 마이페이지에서 확인하실 수 있습니다.'
        );
        onConfirm?.();
        onClose?.();
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [applyId, selectedMethod, selectedAccount, onConfirm, onClose]);

  const onClickReAuth = useCallback(() => {
    if (bankAuthPopup) {
      return;
    }

    const width = '500';
    const height = '600';
    const left = Math.ceil((window.screen.width - width) / 2);
    const top = Math.ceil((window.screen.height - height) / 2);
    const options = `top=${top}, left=${left}, width=${width}, height=${height}, status=no, menubar=no, toolbar=no, resizable=no, location=no, directories=no`;
    const popup = window.open(
      `${process.env.REACT_APP_OPEN_BANK_HOST}/oauth/2.0/authorize` +
        '?response_type=code' +
        `&client_id=${process.env.REACT_APP_OPEN_BANK_CLIENT_ID}` +
        `&redirect_uri=${process.env.REACT_APP_OPEN_BANK_REDIRECT}` +
        '&scope=login+transfer' +
        '&state=b80BLsfigm9OokPTjy03elbJqRHOfGSY' +
        '&auth_type=0',
      null,
      options
    );
    var timer = setInterval(function () {
      if (popup.closed) {
        clearInterval(timer);
        setBankAuthPopup();
      } else if (popup?.location?.pathname == '/openbank/auth') {
        setBankAuthCode(new URLSearchParams(popup.location.search).get('code'));
        popup.close();
      }
    }, 100);
    setBankAuthPopup(popup);
  }, [bankAuthPopup]);

  const onClickReAuthConfirm = useCallback(async () => {
    if (!password) {
      alert('비밀번호를 입력해주세요.');
      return;
    }
    try {
      await reAuthBank(bankAuthCode, password);

      setIsTokenInvalid(false);
      setPassword('');
      setBankAuthCode('');
      try {
        const response = await getMyAccountList();
        alert('인증 정보 등록에 성공했습니다.');
        setAccountList(response.data.accounts);
      } catch (err) {
        if (err.response?.status == 401) {
          setIsTokenInvalid(true);
          return;
        } else if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      }
    } catch (err) {
      if (err.response) {
        alert(err.response.data);
      } else if (err.request) {
        alert('서버가 응답하지 않습니다.');
      } else {
        alert('잘못된 요청입니다.');
      }
    }
  }, [password, bankAuthCode]);

  const onChangePassword = useCallback(({ target }) => {
    setPassword(target.value);
  }, []);

  const onClickResetAuth = useCallback(() => {
    setPassword('');
    setBankAuthCode('');
  }, []);

  return createPortal(
    <div className='PaymentModal_back' onClick={onClickBack}>
      <div className='PaymentModal_root' onClick={(e) => e.stopPropagation()}>
        <div className='PaymentModal_bar'>
          <div className='PaymentModal_bar-side' />
          <span>대금 결제</span>
          <div className='PaymentModal_bar-side' onClick={onClose}>
            <FontAwesomeIcon icon={faXmark} />
          </div>
        </div>
        <div className='PaymentModal_content-root'>
          <div className='PaymentModal_title'>
            <span>결제 방식</span>
          </div>
          <div className='PaymentModal_categories'>
            {payMethodList.map((v) => (
              <div
                className={
                  'PaymentModal_menu-button' +
                  (selectedMethod == v.code
                    ? ' PaymentModal_menu-button-selected'
                    : '')
                }
                onClick={() => onClickMethod(v.code)}
                key={v.code}
              >
                <div className='PaymentModal_menu-button-underline' />
                <span>{v.label}</span>
              </div>
            ))}
          </div>
          {selectedMethod == 2 &&
            (isTokenInvalid ? (
              <div className='PaymentModal_invalid-bank'>
                {bankAuthCode ? (
                  <div>
                    <p>새로 발급받은 인증코드를 계정에 등록합니다.</p>
                    <p>
                      계정 본인 확인을 위해 비밀번호를 입력하고 확인버튼을
                      눌러주세요.
                    </p>
                    <div className='PaymentModal_pwd-box'>
                      <TextField
                        label='비밀번호'
                        type='password'
                        value={password}
                        onChange={onChangePassword}
                      />
                      <Button
                        className='PaymentModal_btn'
                        variant='contained'
                        onClick={onClickReAuthConfirm}
                      >
                        확인
                      </Button>
                      <Button
                        className='PaymentModal_btn-red'
                        variant='contained'
                        onClick={onClickResetAuth}
                      >
                        처음부터 다시 인증하기
                      </Button>
                    </div>
                  </div>
                ) : (
                  <div>
                    <p>회원님의 계좌 인증 정보가 더이상 유효하지 않습니다.</p>
                    {bankAuthPopup ? (
                      <p>이 화면을 닫지 마시고 인증을 마쳐주시기 바랍니다.</p>
                    ) : (
                      <div>
                        <Button
                          className='PaymentModal_btn'
                          variant='contained'
                          onClick={onClickReAuth}
                        >
                          다시 인증하기
                        </Button>
                      </div>
                    )}
                  </div>
                )}
              </div>
            ) : (
              <div className='PaymentModal_title'>
                <span>고객님의 계좌 목록</span>
                <div className='PaymentModal_list'>
                  {accountList.map((v, i) => (
                    <div>
                      <ListItem
                        key={i}
                        component='div'
                        disablePadding
                        className={
                          selectedAccount == v
                            ? 'PaymentModal_selected-account'
                            : ''
                        }
                        onClick={() => setSelectedAccount(v)}
                      >
                        <ListItemButton>
                          <ListItemText
                            primary={`${v.bankName} / ${v.accountNumMasked} / ${v.accountHolderName}`}
                          />
                        </ListItemButton>
                      </ListItem>
                    </div>
                  ))}
                </div>
              </div>
            ))}
        </div>
        <div className='PaymentModal_bottom-bar'>
          <Button
            className='PaymentModal_finalbtn'
            variant='contained'
            onClick={onClickConfirm}
          >
            결제하기
          </Button>
        </div>
      </div>
    </div>,
    document.getElementById('backClickModal')
  );
}

export default PaymentModal;
