import './RequestItem.css';

import React, { useCallback, useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import Chip from '@mui/material/Chip';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import IconButton from '@mui/material/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMessage } from '@fortawesome/free-solid-svg-icons';
import requestStateList from '../../types/requestStateList';
import categoryList from '../../types/category';
import moment from 'moment-timezone';
import { timeForToday } from '../../util/utils';
import { createChatRoom } from '../../_redux/actions/modal_action';
import {
  reportRequest,
  refreshRequest,
  deleteRequest,
  completeRequest,
  waitingRequest,
  hideRequest,
  blockRequest,
} from '../../axios/axios_actions';

function RequestItem({
  requestInfo,
  onChangeRequestInfo,
  onDelete,
  onRefresh,
  onHide,
  onBlock,
}) {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const userInfo = useSelector((state) => state.user.userInfo);

  const [state, setState] = useState();
  const [category, setCategory] = useState();
  const [isMine, setIsMine] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  useEffect(() => {
    if (!requestInfo) {
      return;
    }
    setState(requestStateList.find((v) => v.code == requestInfo.state)?.label);
    setCategory(
      categoryList.find((v) => v.code == requestInfo.category)?.label
    );
    setIsMine(!userInfo?.isAdmin && userInfo?.id == requestInfo.writerInfo.id);
  }, [requestInfo]);

  const onFallbackImg = useCallback((event) => {
    event.currentTarget.onerror = null;
    event.currentTarget.src = '/user.png';
  });

  const handleClick = useCallback((event) => {
    setAnchorEl(event.currentTarget);
  }, []);

  const handleClose = useCallback(() => {
    setAnchorEl(null);
  }, []);

  const onClickMessage = useCallback(() => {
    if (!userInfo) {
      return alert('로그인을 해주세요');
    }
    dispatch(createChatRoom(requestInfo?.writerInfo.id));
  }, [requestInfo]);

  const onClickModify = useCallback(() => {
    handleClose();
    if (!window.confirm('수정 화면으로 이동하시겠습니까?')) {
      return;
    }
    navigate(`/modify?id=${requestInfo.requestId}`);
  }, [requestInfo]);

  const onClickDelete = useCallback(() => {
    handleClose();
    if (!window.confirm('정말 삭제하시겠습니까?')) {
      return;
    }

    deleteRequest(requestInfo?.requestId)
      .then(() => {
        alert('의뢰글을 삭제했습니다.');
        onDelete?.(requestInfo);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [requestInfo]);

  const onClickComplete = useCallback(() => {
    handleClose();
    if (!window.confirm('완료 상태로 변경하시겠습니까?')) {
      return;
    }

    completeRequest(requestInfo?.requestId)
      .then(() => {
        alert('완료로 변경되었습니다.');
        onChangeRequestInfo({ ...requestInfo, state: 30 });
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [requestInfo]);

  const onClickWaiting = useCallback(() => {
    handleClose();
    if (!window.confirm('모집중 상태로 변경하시겠습니까?')) {
      return;
    }

    waitingRequest(requestInfo?.requestId)
      .then(() => {
        alert('모집중으로 변경되었습니다.');
        onChangeRequestInfo({ ...requestInfo, state: 10 });
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [requestInfo]);

  const onClickRefresh = useCallback(() => {
    handleClose();

    refreshRequest(requestInfo?.requestId)
      .then(() => {
        alert('의뢰글을 끌어올렸습니다.');
        onRefresh?.(requestInfo);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [requestInfo]);

  const onClickReport = useCallback(() => {
    handleClose();
    if (!window.confirm('이 의뢰글을 신고하시겠습니까?')) {
      return;
    }

    reportRequest(requestInfo?.requestId)
      .then(() => alert('의뢰글을 신고했습니다.'))
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [requestInfo]);

  const onClickHide = useCallback(() => {
    handleClose();
    if (!window.confirm('이 의뢰글을 신고 목록에서 제외하시겠습니까?')) {
      return;
    }

    hideRequest(requestInfo?.requestId)
      .then(() => {
        alert('의뢰글을 신고 목록에서 제외했습니다.');
        onHide?.(requestInfo);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [requestInfo]);

  const onClickBlock = useCallback(() => {
    handleClose();
    if (!window.confirm('이 의뢰글을 불량글로 블럭 처리하시겠습니까?')) {
      return;
    }

    blockRequest(requestInfo?.requestId)
      .then(() => {
        alert('의뢰글이 블럭되었습니다.');
        onBlock?.(requestInfo);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [requestInfo]);

  return (
    <div className='RequestItem_root'>
      <Link
        to={`/view?id=${requestInfo?.requestId}`}
        className='RequestItem_title-box'
      >
        <span className='RequestItem_state'>{state}</span>
        <span className='RequestItem_title'>{requestInfo?.title}</span>
      </Link>
      <div className='RequestItem_profile'>
        <div className='RequestItem_profile-image-box'>
          <div className='RequestItem_rounding-profile-image'>
            <img src='' onError={onFallbackImg} />
          </div>
        </div>
        <div className='RequestItem_profile-detail-box'>
          <div className='RequestItem_profile-name-box'>
            <span className='RequestItem_profile-name'>
              {requestInfo?.writerInfo?.nickname}
            </span>
            {!userInfo?.isAdmin && !isMine && (
              <FontAwesomeIcon
                icon={faMessage}
                className='RequestItem_message-icon'
                onClick={onClickMessage}
              />
            )}
          </div>
          <div className='RequestItem_address-box'>
            <span className='RequestItem_address'>{requestInfo?.address1}</span>
            <LocationOnIcon className='RequestItem_location-icon' />
          </div>
        </div>
      </div>
      <div className='RequestItem_condition'>
        <div className='RequestItem_condition-left'>
          {requestInfo?.isCanSuggestPayment ? (
            <Chip
              className='RequestItem_suggest-O'
              label='초과 제안 가능'
              color='primary'
              variant='outlined'
            />
          ) : (
            <Chip
              className='RequestItem_suggest-N'
              label='초과 제안 불가'
              color='primary'
              variant='outlined'
            />
          )}
          <Chip
            className='RequestItem_payment'
            label={'₩ ' + requestInfo?.payment.toLocaleString('ko-KR')}
            color='primary'
            variant='contained'
          />
          {requestInfo?.deadline ? (
            <Chip
              className='RequestItem_deadLine'
              label={moment(requestInfo.deadline).format(
                'YYYY/MM/DD' + ' 까지'
              )}
              color='primary'
              variant='contained'
            />
          ) : (
            <Chip
              className='RequestItem_deadLine'
              label='만료기한 없음'
              color='primary'
              variant='contained'
            />
          )}
          <Chip
            className='RequestItem_category'
            label={category}
            color='primary'
            variant='contained'
          />
        </div>
        <div className='RequestItem_condition-right'>
          {userInfo && (
            <>
              <IconButton
                aria-label='more'
                className='RequestItem_long-button'
                aria-controls={open ? 'long-menu' : undefined}
                aria-expanded={open ? 'true' : undefined}
                aria-haspopup='ture'
                onClick={handleClick}
              >
                <MoreVertIcon />
              </IconButton>
              <Menu
                MenuListProps={{ 'aria-labelledby': 'long-button' }}
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
              >
                {userInfo?.isAdmin ? (
                  <div>
                    <MenuItem onClick={onClickBlock}>불량글 제재하기</MenuItem>
                    <MenuItem onClick={onClickHide}>
                      신고 목록에서 감추기
                    </MenuItem>
                  </div>
                ) : isMine ? (
                  <div>
                    <MenuItem onClick={onClickRefresh}>끌어올리기</MenuItem>
                    {requestInfo?.state == 10 && (
                      <MenuItem onClick={onClickComplete}>완료로 변경</MenuItem>
                    )}
                    {requestInfo?.state == 30 && (
                      <MenuItem onClick={onClickWaiting}>
                        모집중으로 변경
                      </MenuItem>
                    )}
                    <MenuItem onClick={onClickModify}>수정하기</MenuItem>
                    <MenuItem onClick={onClickDelete}>삭제하기</MenuItem>
                  </div>
                ) : (
                  <div>
                    <MenuItem onClick={onClickReport}>신고하기</MenuItem>
                  </div>
                )}
              </Menu>
            </>
          )}
        </div>
      </div>
      <div className='RequestItem_view-count-box'>
        <time className='reg_time'>
          {requestInfo?.modTime
            ? `${timeForToday(requestInfo?.modTime)} 수정됨`
            : timeForToday(requestInfo?.regTime)}
        </time>{' '}
        ∙ 조회
        {' ' + requestInfo?.viewCount}
      </div>
    </div>
  );
}

export default RequestItem;
