import './SearchFilterModal.css';

import React, { useCallback, useEffect } from 'react';
import { createPortal } from 'react-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import categories from '../../types/category';
import TextField from '@mui/material/TextField';

function SearchFilterModal({
  selectedCategories,
  minPrice,
  maxPrice,
  toggleCategory,
  onChangeMinPrice,
  onChangeMaxPrice,
  closeWhenClickBack,
  onClose,
}) {
  useEffect(() => {
    document.body.style.overflow = 'hidden';
    return () => {
      document.body.style.overflow = 'unset';
    };
  }, []);

  const onClickBack = useCallback(
    (event) => {
      event.stopPropagation();
      closeWhenClickBack && onClose?.();
    },
    [closeWhenClickBack, onClose]
  );

  const onClickCategory = useCallback(
    (code) => {
      toggleCategory?.(code);
    },
    [toggleCategory]
  );

  return createPortal(
    <div className='SearchFilterModal_back' onClick={onClickBack}>
      <div
        className='SearchFilterModal_root'
        onClick={(e) => e.stopPropagation()}
      >
        <div className='SearchFilterModal_bar'>
          <div className='SearchFilterModal_bar-side' />
          <span>필터 설정하기</span>
          <div className='SearchFilterModal_bar-side' onClick={onClose}>
            <FontAwesomeIcon icon={faXmark} />
          </div>
        </div>
        <div className='SearchFilterModal_content-root'>
          <div className='SearchFilterModal_title'>
            <span>카테고리</span>
          </div>
          <div className='SearchFilterModal_categories'>
            {categories.map((v) => (
              <div
                className={
                  'SearchFilterModal_menu-button' +
                  (selectedCategories?.has(v.code)
                    ? ' SearchFilterModal_menu-button-selected'
                    : '')
                }
                onClick={() => onClickCategory(v.code)}
                key={v.code}
              >
                <div className='SearchFilterModal_menu-button-underline' />
                <span>{v.label}</span>
              </div>
            ))}
          </div>
          <div className='SearchFilterModal_title'>
            <span>희망 가격</span>
          </div>
          <div className='SearchFilterModal_sub'>
            <TextField
              label='최소'
              type='number'
              sx={{ mr: 2, mb: 1 }}
              value={minPrice}
              onChange={onChangeMinPrice}
            />
            <TextField
              label='최대'
              type='number'
              value={maxPrice}
              onChange={onChangeMaxPrice}
            />
          </div>
        </div>
      </div>
    </div>,
    document.getElementById('backClickModal')
  );
}

export default SearchFilterModal;
