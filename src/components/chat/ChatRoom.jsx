import './ChatRoom.css';

import React, { useEffect, useState, useCallback, useRef } from 'react';
import ChatItem from './ChatItem';
import { useSelector } from 'react-redux';
import { useInView } from 'react-intersection-observer';
import Loader from '../common/Loader';
import SockJS from 'sockjs-client';
import * as StompJs from '@stomp/stompjs';
import { getChatMsgList, readChatRoom } from '../../axios/axios_actions';

function ChatRoom() {
  const userInfo = useSelector((state) => state.user.userInfo);
  const chatRoomInfo = useSelector((state) => state.modal.chatRoomInfo);

  const [inputText, setInputText] = useState('');
  const [chatList, setChatList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [lastId, setLastId] = useState();
  const [isEnd, setIsEnd] = useState(false);
  const [topRef, inView] = useInView();
  const [scrollFlag, setScrollFlag] = useState(true);
  const [stompClient, setStompClient] = useState();

  const chatListEm = useRef();

  useEffect(() => {
    if (!chatRoomInfo) {
      return;
    }
    setInputText('');
    setChatList([]);
    setIsLoading(false);
    setLastId();
    setIsEnd(false);

    const client = new StompJs.Client();
    client.webSocketFactory = function () {
      return new SockJS(process.env.REACT_APP_BACKEND_HOST + '/ws');
    };
    client.onConnect = () => {
      client.subscribe(
        `/ws/topic/${userInfo.id}/chat/${chatRoomInfo.id}`,
        (msg) => {
          const parsedMsg = JSON.parse(msg.body);
          setChatList((chatList) => [parsedMsg, ...chatList]);
          if (parsedMsg.senderId == userInfo.id) {
            setInputText('');
            setScrollFlag((scrollFlag) => !scrollFlag);
          } else {
            readChatRoom(chatRoomInfo.id);
          }
        }
      );
    };
    client.activate();
    setStompClient(client);

    return () => {
      client.deactivate();
    };
  }, [chatRoomInfo]);

  // useEffect(() => {
  //   new Promise((resolve, reject) => {
  //     resolve({
  //       data: [],
  //     });
  //   }).then((response) => {
  //     setChatList(response.data);
  //   });
  // }, [chatRoomInfo]);

  useEffect(async () => {
    setIsLoading(true);
    await new Promise((resolve) => setTimeout(resolve, 500));
    try {
      const response = await getChatMsgList(chatRoomInfo.id, lastId);
      setChatList((chatList) => [...chatList, ...response.data.content]);
      setIsEnd(response.data.last);
    } catch {}
    await new Promise((resolve) => setTimeout(resolve, 500));
    setIsLoading(false);
  }, [chatRoomInfo, lastId]);

  useEffect(() => {
    if (inView && !isLoading && !isEnd) {
      setLastId(chatList[chatList.length - 1]?.id);
    }
  }, [inView, isLoading, isEnd, chatList]);

  useEffect(() => {
    chatListEm.current.scrollIntoView({ behavior: 'smooth' });
  }, [scrollFlag]);

  const onChangeInputText = useCallback((event) => {
    setInputText(event.target.value);
  });

  const onKeyUpInputText = useCallback(
    (event) => {
      if (event.keyCode == 13) {
        event.preventDefault();
        stompClient.publish({
          destination: '/ws/api/user/chat',
          body: JSON.stringify({
            receiverId: chatRoomInfo.partnerId,
            message: inputText,
          }),
        });
        return;
      }
    },
    [inputText, stompClient]
  );

  return (
    <main className='ChatRoom_root'>
      <div className='ChatRoom_list'>
        <div ref={chatListEm} />
        {chatList.map((v, idx) => (
          <ChatItem chatInfo={v} key={idx} />
        ))}
        <div ref={topRef} className='ChatRoom_list-top'>
          {isLoading && <Loader size='32px' />}
        </div>
      </div>
      <div className='ChatRoom_bottom'>
        <input
          type='text'
          value={inputText}
          onChange={onChangeInputText}
          onKeyUp={onKeyUpInputText}
        />
      </div>
    </main>
  );
}

export default ChatRoom;
