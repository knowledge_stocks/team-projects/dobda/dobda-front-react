import React from 'react';
import ChatRoomListModal from './ChatRoomListModal';
import ChatRoomModal from './ChatRoomModal';
import { useSelector } from 'react-redux';

function ChatModalHandler() {
  const isShowChatRoomList = useSelector(
    (state) => state.modal.isShowChatRoomList
  );
  const chatRoomInfo = useSelector((state) => state.modal.chatRoomInfo);

  return (
    <>
      {isShowChatRoomList && <ChatRoomListModal />}
      {chatRoomInfo && <ChatRoomModal />}
    </>
  );
}

export default ChatModalHandler;
