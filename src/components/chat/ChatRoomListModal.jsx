import './ChatRoomListModal.css';

import React, { useCallback, useEffect } from 'react';
import { createPortal } from 'react-dom';
import ChatRoomList from './ChatRoomList';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import { closeChatRoomList } from '../../_redux/actions/modal_action';
import { useDispatch } from 'react-redux';

function ChatRoomListModal() {
  const dispatch = useDispatch();

  useEffect(() => {
    return () => {
      document.body.style.overflow = 'unset';
    };
  });

  const close = useCallback((event) => {
    event.preventDefault();
    dispatch(closeChatRoomList());
  }, []);

  return createPortal(
    <div
      className='ChatRoomListModal_root'
      onMouseEnter={() => (document.body.style.overflow = 'hidden')}
      onMouseLeave={() => (document.body.style.overflow = 'unset')}
    >
      <div className='ChatRoomListModal_bar'>
        <span>채팅방 목록</span>
        <div onClick={close}>
          <FontAwesomeIcon icon={faXmark} />
        </div>
      </div>
      <div className='ChatRoomListModal_list'>
        <ChatRoomList />
      </div>
    </div>,
    document.getElementById('chatRoomListModal')
  );
}

export default ChatRoomListModal;
